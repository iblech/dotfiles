{-# LANGUAGE FlexibleContexts #-}

import XMonad
import XMonad.StackSet
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.EwmhDesktops
import XMonad.Layout.Gaps
import XMonad.Util.Run
import XMonad.Util.EZConfig(additionalKeys)
import System.IO
import qualified Data.Map as M
import Data.IORef
import Control.Monad
import XMonad.Layout.NoBorders
import XMonad.Actions.SpawnOn

import XMonad.Prompt
import Text.Read
import Data.Char
import Data.List

main = do
    batterySavingMode <- io $ newIORef False
    st0Ref <- io $ newIORef False
    st1Ref <- io $ newIORef $ M.fromList [("1", True), ("2", False), ("3", False), ("4", True)]
    let sw w = do
                targetSt <- liftM (M.lookup w) (io $ readIORef st1Ref)
                curSt    <- io $ readIORef st0Ref
                when ((targetSt == Just False && curSt == True) || (targetSt == Just True && curSt == False)) $ do
                    spawn "xrandr-invert-colors"
                    io $ modifyIORef st0Ref not
                whenM (io $ readIORef batterySavingMode) $
                    if w == "1"
                        then spawn "/home/iblech/.bin/battery.sh run"
                        else spawn "/home/iblech/.bin/battery.sh pause"
                windows . greedyView $ w
    let inv = do
                w <- gets $ currentTag . windowset
                io $ modifyIORef st0Ref not
                newSt <- io $ readIORef st0Ref
                io $ modifyIORef st1Ref $ M.insert w newSt
                spawn "xrandr-invert-colors"
    let redshift doRed = do
                            w <- gets $ currentTag . windowset
                            st <- liftM (M.lookup w) (io $ readIORef st1Ref)
                            spawn $ concat
                                [ "/home/user/.bin/redshift.sh "
                                , if st == Just True then "1" else "0"
                                , " "
                                , if doRed then "1" else "0"
                                ]
    let startup = do
		    sw "2"
		    redshift True  -- sollte initial auf einer nicht-invertierten Arbeitsfläche passieren
		    spawn "/home/user/.bin/x11.sh"
		    spawn "/home/user/.bin/battery-log.sh"
		    spawnOn "3" "urxvt -e /home/user/.bin/m19.sh"
		    spawnOn "2" "urxvt -e screen -UaRD"
		    spawnOn "4" "telegram-desktop"
		    spawnOn "4" "element-desktop"
		    -- sendMessage ToggleGaps
		    rescreen
    xmonad $ ewmh $ def
        -- { layoutHook  = gaps [(D, 32)] $ noBorders Full ||| Tall 1 (3/100) (2/3)
        { layoutHook  = noBorders Full ||| Tall 1 (3/100) (2/3)
        , startupHook = startup
        , manageHook  = manageSpawn <+> manageHook def
        , modMask     = mod4Mask
        } `additionalKeys`
        [ ((mod4Mask, xK_Up),     sw "1")
        , ((mod4Mask, xK_Left),   sw "3")
        , ((mod4Mask, xK_Right),  sw "4")
        , ((mod4Mask, xK_Down),   sw "2")
        , ((mod4Mask .|. shiftMask, xK_Return), spawn "urxvt")
        , ((mod4Mask, xK_i),      inv)
        , ((mod4Mask, xK_j),      inv)
        , ((mod4Mask, xK_g),      sendMessage ToggleGaps)
        , ((mod4Mask, xK_a),      spawn "urxvt -e alsamixer")
        , ((mod4Mask, xK_r),      redshift True)
        , ((mod4Mask .|. shiftMask, xK_r), redshift False)
        , ((mod4Mask, xK_s),      spawn "urxvt -e screen -UaRD")
        , ((mod4Mask, xK_m),      spawn "urxvt -e /home/user/.bin/m19.sh")
        , ((mod4Mask, xK_p),      spawn "urxvt -e /home/user/.bin/mpv-clip.sh")
        , ((mod4Mask, xK_w),      spawn "/home/user/.bin/browser.sh")
        , ((mod4Mask .|. shiftMask, xK_w), spawn "/home/user/.bin/browser-qcoh.sh")
        , ((mod4Mask .|. shiftMask, xK_s), io $ modifyIORef batterySavingMode not)
        , ((mod1Mask, xK_Tab),    windows focusDown)
        , ((mod1Mask .|. shiftMask, xK_Tab),   windows focusUp)
        , ((mod4Mask, xK_u),      rescreen)
        ]

whenM p m = p >>= \p' -> when p' m

keyPrompt :: XPConfig -> X ()
keyPrompt config = mkXPrompt Key config (const $ return []) (paste . readMaybe)
    where
    paste (Just code) = safeSpawn "xdotool" ["type", [chr (code - ord 'A' + ord 'a')]]
    paste Nothing     = return ()

data Key = Key
instance XPrompt Key where
    showXPrompt Key = (++ ": ") . concat . intersperse " " $
        map (\c -> c : show (ord c)) ['A'..'Z']
