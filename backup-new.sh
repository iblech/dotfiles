#!/usr/bin/env bash

set -ex

tar --exclude=.cache --exclude=.dbus -cvzf /home/iblech/ethercalc-backup.tgz /ethercalc /home-boxes/*adova*

timestamp=$(date -u -Iseconds)

btrfs subvolume snapshot -r /home/iblech /home/snapshots/$(date -u -Iseconds)

ssh -t root@quasilila "
  set -e

  [ -e /dev/mapper/home ] || cryptsetup luksOpen /dev/sda7 home
  [ -e /mnt/snapshots ]   || mount /dev/mapper/home /mnt

  cd /mnt/snapshots
  base=\$(ls -1 | tail -n 1)
  test -d \$base

  btrfs su snapshot \$base .$timestamp
"

rsync --bwlimit=1.5m --delete -avz /home/snapshots/$timestamp/ root@quasilila:/mnt/snapshots/.$timestamp

ssh root@quasilila "
  cd /mnt/snapshots
  mv .$timestamp $timestamp
"
