{ config, pkgs, fetchurl, lib, ... }:

let
  mytexlive = pkgs.texlive.combine {
    inherit (pkgs.texlive) scheme-basic biblatex logreq xstring csquotes tabto-ltx
    soul xypic libertine chngcntr multirow bussproofs breakurl mweights
    fontaxes environ framed trimspaces adjustbox collectbox tikz-cd tkz-base tkz-euclide numprint comment
    moderncv fontawesome ifsym a4wide cleveref arev shadethm blindtext
    todonotes mdframed bbding titlesec wrapfig makecell enumitem nopageno
    needspace stix subfigure footmisc lkproof ocgx2 media9 ucs dirtytalk
    tcolorbox tikzpagenodes ifoddpage fdsymbol kurier preview cjk dashrule
    ifmtarg eepic multibib graphbox datatool xfor substr pgfplots
    draftwatermark everypage minted newunicodechar cancel svg ifplatform
    mnsymbol cmll endfloat xpatch fvextra upquote calligra accents
    boxedminipage animate zref marginnote bbm ntheorem type1cm newtx titling
    halloweenmath pict2e fontawesome5 arydshln catchfile manfnt
    endnotes ncctools esint thmtools silence bbm-macros sttools censor pbox tokcycle
    beamer ulem mathtools booktabs stmaryrd ragged2e metafont koma-script;
  };
  onionName = lib.removeSuffix "\n" (builtins.readFile "/root/myonion");
in

{
  system.stateVersion = "20.09";

  imports =
    [ ./hardware-configuration.nix
    ];

  nix.settings.sandbox = true;
  #nix.package = pkgs.nixFlakes;
  nix.extraOptions = ''
    experimental-features = nix-command flakes
  '';

#  services.ethercalc = {
#    enable = true;
#    package = pkgs.callPackage /tmp-iblech/nixpkgs/pkgs/servers/web-apps/ethercalc {};
#  };

  boot.loader.grub.enable = true;
  boot.loader.timeout = 5;
  boot.loader.grub.device = "/dev/sda";
  boot.tmp.useTmpfs = true;

  boot.kernel.sysctl = {
    "vm.dirty_writeback_centisecs" = 6000;
    "fs.inotify.max_user_watches" = 163840;
    "fs.inotify.max_user_instances" = 2048;
    "fs.file-max" = 655360;
  };
  boot.kernelPackages = pkgs.linuxPackages_latest;
  boot.kernelParams = [ "panic=10" "boot.shell_on_fail" ];
  services.journald.extraConfig = "SystemMaxUse=200M";

  boot.initrd.systemd.enable = true;
  boot.initrd.network = {
    enable = true;
    ssh = {
      enable = true;
      port = 2222;
      authorizedKeys = [
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCuo33kfHx42dfPPIzwV9b80LFSANPxsehOzXsVQTv9hcNuv8AagK9XKy12V8i1HYHZtqTLfE4EO1bVss18IvCoSA/0kuvB2Cq/aPMbq4Lu+c9UvL1YNIFb5nvH6g+/hpQOHqJfaH687cuWq4cOfIjSHVwuHwScgqJYE0sIsyb51GbyBF7OeY5I0ZNCPK7CZNO5MfQkvjERCmmgeYLXG7TDY+fNUToAfP9ZpVjgNx98t3fUoi0ydaoIaWGh0o+l2wUV8AhOCA4mYJFrtBudawt7z8u4yfNt54+HO5Z+ZQ90zqzH83lqkopthRiQyVcltnWbmXkBe17kyYujyNIMdjD81hEH/px/m8PuyK+K9ElOZGwFNEa5hsDvYExNJR93w3NjCkUGJLW4KwEkxc/eqem00p8oEliSCXGqtLGLSg4c9Dv49E0RApqwTaUIwd53v+/PVv0yIpdXrY/9279dDUXlOxmdVQRdM8CRkDzuO/bLCFH443eFN6ClhBnY8oiwuUU= user@lila"
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDZD8s13pFcQsztNc5ocSqNDDNDmHvfDH9jUZUBZ8JGuOIHc1zPkxPusUmLftcnWW3c6r5TMHFw2mSciAx7KDmYU0DkKLLN9KiBF1+fLLXvKtHpT2Mvl1mZAsGoHFuh3M+Nbdtpgn0nbBh7Nv1P8CbU7Y2ICJsWEUNfvMFruFIIfv+r5xJ5HYsW5Lj7u4/DNZMi080y9MpC5hFS1UVYcn6vgk7TWtKkKpjp3RFNIb1jzmEPXrfPE6Nf11snp39B8ZZUHJIa04RWkewsqRiSspCzdI6CjMVWv/vDuegp1BV2P8zdkmV0GHDaN+TE+eWIxGd063zfpmhbzIOny486/XAUVve2IdjIigH7tcK7r/TiKDEe5W00m2DLVto73xr2mXuWD8Nx1wCBan7hhJFhsE/R/OqfsfeUdW6PvMFb428cpwAqczApZOv3AHlzOGVIWUj9gAmdZTvSr2oFlTe/8elvRtxpATZmgXaKuno8+QaOUHiOmJCjh9YQWceHo1hoQEk= u0_a141@localhost"
      ];
      hostKeys = [ /etc/nixos/initrd_key ];
    };
  };

  networking.hostName = "quasitopos";
  networking.useNetworkd = true;
  networking.nameservers = [ "1.1.1.1#one.one.one.one" "1.0.0.1#one.one.one.one" ];
  services.resolved = {
    enable = true;
    dnssec = "false";
    domains = [ "~." ];
    fallbackDns = [ "1.1.1.1#one.one.one.one" "1.0.0.1#one.one.one.one" ];
    dnsovertls = "true";
  };

  time.timeZone = "Europe/Berlin";
  services.timesyncd.servers = [ "0.nixos.pool.ntp.org" ];

  i18n = {
    defaultLocale = "en_US.UTF-8";
    extraLocaleSettings.LC_TIME = "C.UTF-8";
  };

  systemd.network.enable = true;
  systemd.network.networks."10-wan" = {
    matchConfig.Name = "enp1s0";
    networkConfig = {
      DHCP = "ipv4";
      IPv6AcceptRA = true;
    };
    linkConfig.RequiredForOnline = "routable";
  };

  services.openssh.enable = true;
  services.openssh.ports = [ 223 53 ];
  services.openssh.settings.PermitRootLogin = "yes";
  services.openssh.settings.X11Forwarding = true;
  services.openssh.extraConfig = ''
    AcceptEnv LANG LC_*
    TCPKeepAlive yes

    Match User giulio
      X11Forwarding no
      AllowTcpForwarding no
      ChrootDirectory /websites/pacman.logicverona.it
      ForceCommand internal-sftp
  '';

  networking.firewall.enable = false;

  environment.systemPackages = with pkgs; [
    wget bc vim man-pages screen file mosh unzip parallel binutils-unwrapped
    btdu
    perl
    psmisc pciutils usbutils lsof powertop
    git tig
    nmap iptables ethtool dnsutils arp-scan socat
    mutt procmail fetchmail
    sshfs
    lynx elinks
    cryptsetup (nbd.overrideAttrs (_: { doCheck = false; }))
    gnupg
    aspell aspellDicts.de aspellDicts.en
    mytexlive biber
    pkgs.gedit
    mumble pavucontrol #chromium
    xterm firefox icewm
    xorg.libxcb xorg.xauth
    xbill
    yt-dlp ffmpeg #-full
    tigervnc
    python3
    #zoom-us
    mumble
    imagemagick
    mpv
    #pitivi
    tdesktop feh inkscape evince mupdf
    #libreoffice gimp
    gnuplot
    casync
    ripgrep
    borgbackup
    docker-compose
  ];
  nixpkgs.config.allowUnfree = true;
  hardware.pulseaudio.enable = true;
  hardware.graphics.enable = true;

  fonts.packages = with pkgs; [
    hack-font ubuntu_font_family
    fira-code cascadia-code
    corefonts
  ];

  environment.variables = {
    EDITOR = "${pkgs.vim}/bin/vim";
  };

  programs.bash.completion.enable = false;
  programs.bash.interactiveShellInit = ''
    HISTSIZE=10000
    HISTCONTROL=ignorespace:ignoredups
    HISTFILESIZE=200000
  '';

  users.groups = { iblech = { gid = 1000; }; };
  users.users.iblech = {
    isNormalUser = true;
    description = "Ingo Blechschmidt";
    home = "/home/iblech";
    uid = 1000;
    group = "iblech";
    extraGroups = [ "audio" ];
  };

  users.users.giulio = {
    isNormalUser = true;
    home = "/websites/pacman.logicverona.it/wwwroot";
    shell = null;
    homeMode = "755";
  };

  systemd = {
    services.closeluks = {
      serviceConfig.Type = "oneshot";
      script = ''
        if ${pkgs.util-linux}/bin/umount /dev/mapper/crypt-home 2>/dev/null; then
          ${pkgs.cryptsetup}/bin/cryptsetup luksClose crypt-home
        fi
      '';
    };

    timers.closeluks = {
      wantedBy = [ "timers.target" ];
      partOf = [ "closeluks.service" ];
      timerConfig.OnCalendar = "*:0/2";
    };

    services.webalizer = {
      serviceConfig.Type = "oneshot";
      serviceConfig.User = "nginx";
      script = ''
        for i in klimacamp-augsburg verkehrswende-augsburg endfossilaugsburg kollektivr qualle-augsburg greenoffice-augsburg karlsruhe autofreitag; do
          mkdir -p /webalizer/$i
          ${pkgs.webalizer}/bin/webalizer -n $i -o /webalizer/$i /var/log/nginx/$i.log || true
        done
      '';
    };

    timers.webalizer = {
      wantedBy = [ "timers.target" ];
      partOf = [ "webalizer.service" ];
      timerConfig.OnCalendar = "*-*-* 00:00:00";
    };

    services.git-announcement = {
      serviceConfig.Type = "oneshot";
      serviceConfig.User = "iblech";
      path = with pkgs; [ bash git curl openssh gnugrep ];
      script = ''
        (
          mkdir -p /repositories/synthetic-zariski
          cd /repositories/synthetic-zariski
          /repositories/commit-announce.sh https://github.com/felixwellen/synthetic-zariski $(cat /repositories/telegram-api-key) -1002146179850
        )

        (
          mkdir -p /tmp-iblech/kc-aux
          cd /tmp-iblech/kc-aux
          /tmp-iblech/commit-announce.sh https://gitlab.com/baldrian/klimacamp-augsburg $(cat /tmp-iblech/telegram-api-key) -1001556827502
        )

        (
          mkdir -p /repositories/nlab-deployment
          cd /repositories/nlab-deployment
          /repositories/commit-announce.sh https://github.com/ncatlab/nlab-deployment $(cat /repositories/telegram-api-key) -4001829773
        )
      '';
    };

    timers.git-announcement = {
      wantedBy = [ "timers.target" ];
      partOf = [ "git-announcement.service" ];
      timerConfig.OnCalendar = "*-*-* *:13:00";
    };

    services.do-backup = {
      serviceConfig.Type = "oneshot";
      serviceConfig.User = "root";
      script = ''
        /home/iblech/backup-neo.sh 2>&1 | /home/iblech/pipe-telegram.sh -4144386184 backup-quasitopos-"$(date +%F-%H-%M-%S)".txt "This archive"
        /home/nlab-roots/full-server-backup.sh /home/nlab-roots 2>&1 | /home/iblech/pipe-telegram.sh -4001829773 backup-iblech-"$(date +%F-%H-%M-%S)".txt "Total size of backup"
      '';
      path = with pkgs; [ bash borgbackup curl gnugrep bzip2 openssh btrfs-progs coreutils rsync ];
    };

    timers.do-backup = {
      wantedBy = [ "timers.target" ];
      partOf = [ "do-backup.service" ];
      timerConfig.OnCalendar = "*-*-* 18:00:00";
    };

    sockets.nlab80.listenStreams = [ "81" ];
    sockets.nlab443.listenStreams = [ "444" ];
    services.nlab80 = {
      serviceConfig = {
        ExecStart = "${pkgs.systemd}/lib/systemd/systemd-socket-proxyd 10.0.1.2:80";
      };
      wantedBy = [ "multi-user.target" ];
      requires = [ "nlab80.socket" ];
      after = [ "nlab80.socket" ];
    };
    services.nlab443 = {
      serviceConfig = {
        ExecStart = "${pkgs.systemd}/lib/systemd/systemd-socket-proxyd 10.0.1.2:443";
      };
      wantedBy = [ "multi-user.target" ];
      requires = [ "nlab443.socket" ];
      after = [ "nlab443.socket" ];
    };
  };

  users.users.klimacamp-aux = { isNormalUser = true; description = "KC Aux"; home = "/home/kc-aux"; uid = 20000; };
  containers.klimacamp-aux-rebuild = {
    config =
      { config, pkgs, ... }:
      {
        system.stateVersion = "${config.system.nixos.release}";
        systemd.services.rebuild-klimacamp = {
          description = "rebuild-klimacamp";
          wantedBy = [ "multi-user.target" ];
          serviceConfig = {
            ExecStart = "/klimacamp-augsburg/webhook.sh";
            User = "klimacamp-aux";
          };
          path = let gems = pkgs.bundlerEnv {
            name = "gems-for-klimacamp-augsburg";
            gemdir = /klimacamp-augsburg;
          }; in [ gems gems.wrappedRuby pkgs.bash pkgs.git pkgs.perl ];
        };
        users.users.klimacamp-aux = { isNormalUser = true; description = "KC Aux"; home = "/home-kcaux"; uid = 20000; };
      };
    ephemeral = true;
    autoStart = true;
    bindMounts = {
      "/klimacamp-augsburg-ipc" = { hostPath = "/run/klimacamp-augsburg-ipc"; isReadOnly = false; };
      "/klimacamp-augsburg" = { hostPath = "/klimacamp-augsburg"; isReadOnly = false; };
    };
  };

  systemd.tmpfiles.rules = [
    "d /run/univr-ipc 777 root root -"
    "d /run/klimacamp-augsburg-ipc 777 root root -"
  ];

  containers.univr-rebuild = {
    config =
      { config, pkgs, ... }:
      {
        system.stateVersion = "${config.system.nixos.release}";
        networking.nameservers = [ "8.8.8.8" ];
        systemd.services.rebuild-univr = {
          description = "rebuild-univr";
          wantedBy = [ "multi-user.target" ];
          script = ''
            [ -d /univr-ipc ] || exit 1

            git config --global user.email "ingo.blechschmidt@univr.it"
            git config --global user.name "Ingo Blechschmidt (via Quasicoherent)"
            git config --global push.default simple

            mkdir -m 700 -p $HOME/.ssh
            echo "$SSH_DEPLOY_KEY" | base64 -d > $HOME/.ssh/id_rsa
            chmod 600 $HOME/.ssh/*
            cat >> $HOME/.ssh/known_hosts <<EOF
            github.com ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOMqqnkVzrm0SdG6UOoqKLsabgH5C9okWi0dh2l9GKJl
            github.com ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCj7ndNxQowgcQnjshcLrqPEiiphnt+VTTvDP6mHBL9j1aNUkY4Ue1gvwnGLVlOhGeYrnZaMgRK6+PKCUXaDbC7qtbW8gIkhL7aGCsOr/C56SJMy/BCZfxd1nWzAOxSDPgVsmerOBYfNqltV9/hWCqBywINIR+5dIg6JTJ72pcEpEjcYgXkE2YEFXV1JHnsKgbLWNlhScqb2UmyRkQyytRLtL+38TGxkxCflmO+5Z8CSSNY7GidjMIZ7Q4zMjA2n1nGrlTDkzwDCsw+wqFPGQA179cnfGWOWRVruj16z6XyvxvjJwbz0wQZ75XK5tKSb7FNyeIEs4TT4jk+S4dhPeAUC5y+bDYirYgM4GC7uEnztnZyaVWQ7B381AK4Qdrwt51ZqExKbQpTUNn+EjqoTwvqNj4kqx5QUCI0ThS/YkOxJCXmPUWZbhjpCg56i+2aB6CmK2JGhn57K5mj0MNdBXA4/WnwH6XoPWJzK5Nyu2zB3nAZp+S5hpQs+p1vN1/wsjk=
            github.com ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBEmKSENjQEezOmxkZMy7opKgwFB9nkt5YRrYMjNuG5N87uRgg6CLrbo5wAdT/y6v0mKV0U2w0WZ2YB/++Tpockg=
            EOF

            cd $HOME || exit 1
            [ -d website/.git ] || git clone git@github.com:logic-seminar-verona/website.git
            cd website || exit 1

            while :; do
              rmdir /univr-ipc/rebuild 2>/dev/null && {
                git reset --hard origin/master
                git fetch
                git merge origin/master
                ./deploy.sh git@github.com:logic-seminar-verona/website.git || true
              }

              sleep 2
            done
          '';
          serviceConfig = {
            User = "univr";
            Environment = "SSH_DEPLOY_KEY=${builtins.readFile "/root/univr-ssh-deploy-key"}"; 
          };
          path = with pkgs; [ bash nix wget git openssh (haskellPackages.ghcWithPackages (p: [ p.cabal-install p.hakyll p.pandoc ])) ];
        };
        users.users.univr = { isNormalUser = true; description = "Univr"; home = "/home/univr"; uid = 20002; };
        documentation.doc.enable = false;
      };
    ephemeral = true;
    autoStart = true;
    bindMounts = {
      "/univr-ipc" = { hostPath = "/run/univr-ipc"; isReadOnly = false; };
    };
  };

  services.tor.enable = true;
  services.tor.client.enable = true;

  services.privoxy.enable = true;
  services.privoxy.settings = {
    #forward-socks5t = "/ 127.0.0.1:9050 .";
    toggle = false;
    enable-remote-toggle = false;
    enable-edit-actions = false;
    enable-remote-http-toggle = false;
  };

  systemd.sockets.linuxpad-tor = {
    listenStreams = [ (builtins.readFile "/root/linuxpad-tor-socket-path1") (builtins.readFile "/root/linuxpad-tor-socket-path2") (builtins.readFile "/root/linuxpad-tor-socket-path3") (builtins.readFile "/root/linuxpad-tor-socket-path4") (builtins.readFile "/root/linuxpad-tor-socket-path5") (builtins.readFile "/root/linuxpad-tor-socket-path6") "/torsock" ];
  };
  systemd.services.linuxpad-tor = {
    serviceConfig = {
      ExecStart = "${pkgs.systemd}/lib/systemd/systemd-socket-proxyd 127.0.0.1:8118";
    };
    wantedBy = [ "multi-user.target" ];
    requires = [ "linuxpad-tor.socket" ];
    after = [ "linuxpad-tor.socket" ];
  };

  users.users.guest = { isNormalUser = true; description = "Guest"; home = "/agdapad-homes"; uid = 10000; };

  containers.box = {
    config =
      { config, ... }:
      {
        imports = [ /root/agdapad/backend/container.nix ];
        system.stateVersion = "${config.system.nixos.release}";

        containers.xskeleton.config = {
          hardware.pulseaudio.enable = false;
          environment.systemPackages = with pkgs; [
            file elinks lynx
            bash bastet bc djview evince feh firefox gimp
            git gnuplot gtypist imagemagick inkscape  # golly
            moon-buggy mupdf nano man-pages
            redshift screen socat sshfs texmaker texstudio
            tig unzip vim wget xaos xcalib xdotool xorg.xauth xorg.xev
            xorg.xgamma xorg.xkill xorg.xmessage xorg.xmodmap xorg.xwd
            xorg.xwininfo xorg.libxcb xrandr-invert-colors xsel xvkbd
            (pkgs.python3.withPackages (self: [ self.numpy self.matplotlib self.pygments self.pip ]))
            mytexlive biber
            icewm
            pkgs.gedit geany
            thonny
            #winePackages.full
            mumble pavucontrol
            mono gcc
            ghc
            # enigma blobwars fish-fillets-ng freedroid freedroidrpg #frozen-bubble #krita # stellarium
            # gnujump pingus tuxpaint xbill # jumpnbump
            # racket
            chez binutils
            htop
            #pitivi
            libreoffice
            #rstudio
            zip
            gnuplot
          ];
          services.xserver.desktopManager.xfce.enable = true;
          services.xserver.desktopManager.xfce.enableScreensaver = false;
          fonts.fontconfig.enable = true;
          fonts.packages = with pkgs; [ hack-font ubuntu_font_family corefonts liberation_ttf dejavu_fonts ];
          nixpkgs.pkgs = pkgs;
        };
      };
    ephemeral = true;
    autoStart = true;
    privateNetwork = true;
    hostAddress = "192.168.0.1";
    localAddress = "192.168.0.2";
    bindMounts =
      { "/home" = { hostPath = "/home-boxes"; isReadOnly = false; };
        "/torsock" = { hostPath = "/torsock"; isReadOnly = false; };
      };
  };

  users.users.ethercalc = { isNormalUser = true; description = "Ethercalc"; home = "/ethercalc"; uid = 20001; };
  containers.ethercalc = {
    config =
      { config, pkgs, ... }:
      {
        system.stateVersion = "${config.system.nixos.release}";
        users.users.ethercalc = { isNormalUser = true; description = "Ethercalc"; home = "/home/ethercalc"; uid = 20001; };
        users.groups.ethercalc = { gid = 20001; members = [ "ethercalc" ]; };
        networking.firewall.enable = false;
        systemd.services.ethercalc = {
          serviceConfig = {
            # git clone https://github.com/audreyt/ethercalc
            # nix-shell -p nodePackages.node2nix
            # node2nix -i package.json -l package-lock.json
            ExecStart = "${pkgs.ethercalc}/bin/ethercalc";
            User = "ethercalc";
            Group = "ethercalc";
            WorkingDirectory = "~";
            Restart = "on-failure";
          };
          wantedBy = [ "multi-user.target" ];
        };
      };
    ephemeral = true;
    autoStart = true;
    privateNetwork = true;
    hostAddress = "192.168.2.1";
    localAddress = "192.168.2.2";
    bindMounts = { "/home/ethercalc" = { hostPath = "/ethercalc"; isReadOnly = false; }; };
  };

  systemd.services.nginx.serviceConfig.BindPaths =
    [ "/run/klimacamp-augsburg-ipc"
      "/run/univr-ipc"
    ];
  services.nginx = {
    enable = true;
    #defaultSSLListenPort = 4443;
    package = pkgs.nginxMainline.override {
      modules = with pkgs.nginxModules; [ brotli dav zstd ];
      withPerl = true;
    };
    recommendedTlsSettings = true;
    recommendedGzipSettings = true;
    recommendedOptimisation = true;
    appendHttpConfig = ''
      proxy_send_timeout 600;
      proxy_read_timeout 600;
      proxy_http_version 1.1;
      proxy_cache_path /var/cache/nginx levels=1:2 keys_zone=STATIC:10m inactive=24h max_size=1g;
      server_names_hash_bucket_size 128;
    '';
    commonHttpConfig = ''
      brotli on;
      brotli_static on;
      brotli_types application/json application/javascript application/xml application/xml+rss image/svg+xml text/css text/javascript text/plain text/xml;
      zstd on;
      zstd_types application/json application/javascript application/xml application/xml+rss image/svg+xml text/css text/javascript text/plain text/xml;
      types {
        text/x-agda     agda lagda;
      }
      charset utf-8;
      charset_types text/x-agda;
      map $scheme $hsts_header {
        https "max-age=31536000; includeSubdomains; preload";
      }
      add_header Strict-Transport-Security $hsts_header;
      log_format vhost_combined '$server_name $remote_addr - $remote_user [$time_local] '
				'"$request" $status $body_bytes_sent '
				'"$http_referer" "$http_user_agent"';
      access_log /var/log/nginx/other_vhosts_access.log vhost_combined;
    '';
    virtualHosts."chaos.quasicoherent.io" = {
      forceSSL = true;
      enableACME = true;
      locations."/".root = "/websites/37c3/";
    };
    virtualHosts."linklab.quasicoherent.io" = {
      forceSSL = true;
      enableACME = true;
      locations."/" = {
        proxyPass = "http://172.16.0.2";
        proxyWebsockets = true;
      };
    };
    virtualHosts."agdapad.quasicoherent.io" = {
      forceSSL = true;
      enableACME = true;
      locations."/" = {
        proxyPass = "http://192.168.0.2";
        proxyWebsockets = true;
      };
    };
    virtualHosts."agdatopos.quasicoherent.io" = {
      forceSSL = true;
      enableACME = true;
      locations."/" = {
        proxyPass = "http://192.168.1.2";
        proxyWebsockets = true;
      };
      locations."~ ^/~(\\w+)(\\/.*)?$" = {  # exclude both ".."-style enumeration attacks and access to ".skeleton", ".hot-spare-*" etc.
        alias = "/$1$2";
        extraConfig = ''
          autoindex on;
          dav_methods     PUT DELETE MKCOL COPY MOVE;
          dav_ext_methods PROPFIND OPTIONS;
          dav_access      user:rw group:rw all:r;
        '';
      };
    };
    virtualHosts."passau.klimacamp.eu" = {
      forceSSL = true;
      enableACME = true;
      locations."/".root = "/websites/klimacamp-passau/_site";
      locations."~* \\.(jpg|jpeg|png|gif|svg|webp|ico|css|js)$" = {
        root = "/websites/klimacamp-passau/_site";
        extraConfig = "expires 365d;";
      };
    };
    virtualHosts."www.passau.klimacamp.eu" = {
      enableACME = true;
      addSSL = true;
      locations."/".extraConfig = ''
        return 301 https://passau.klimacamp.eu$request_uri;
      '';
    };
    virtualHosts."www.ravensburg.klimacamp.eu" = {
      enableACME = true;
      addSSL = true;
      locations."/".extraConfig = ''
        return 301 https://ravensburg.klimacamp.eu$request_uri;
      '';
    };
    virtualHosts."www.klimacamp-bayreuth.de" = {
      forceSSL = true;
      enableACME = true;
      locations."/".root = "/websites/klimacamp-bayreuth/_site";
      locations."~* \\.(jpg|jpeg|png|gif|svg|webp|ico|css|js)$" = {
        root = "/websites/klimacamp-bayreuth/_site";
        extraConfig = "expires 365d;";
      };
    };
    virtualHosts."klimacamp-bayreuth.de" = {
      enableACME = true;
      addSSL = true;
      locations."/".extraConfig = ''
        return 301 https://www.klimacamp-bayreuth.de$request_uri;
      '';
    };
    virtualHosts."klimaschutz.madeingermany.lol" = {
      forceSSL = true;
      enableACME = true;
      locations."/" = { root = "/klimacamp-augsburg/pages/madeingermany.lol"; };
    };
    virtualHosts."klimagerechtigkeit.madeingermany.lol" = {
      forceSSL = true;
      enableACME = true;
      locations."/" = { root = "/klimacamp-augsburg/klimagerechtigkeit.madeingermany.lol"; };
      locations."~* \\.(jpg|jpeg|png|gif|svg|webp|ico|css|js|webm|woff2)$" = {
        root = "/klimacamp-augsburg/klimagerechtigkeit.madeingermany.lol";
        extraConfig = "expires 365d;";
      };
    };
    virtualHosts."www.klimagerechtigkeit.madeingermany.lol" = {
      enableACME = true;
      addSSL = true;
      locations."/".extraConfig = ''
        return 301 https://klimagerechtigkeit.madeingermany.lol$request_uri;
      '';
    };
    virtualHosts."kasti.klimacamp.eu" = {
      enableACME = true;
      addSSL = true;
      locations."/".extraConfig = ''
        return 301 https://kastibleibt.noblogs.org$request_uri;
      '';
    };
    virtualHosts."www.kasti.klimacamp.eu" = {
      enableACME = true;
      addSSL = true;
      locations."/".extraConfig = ''
        return 301 https://kastibleibt.noblogs.org$request_uri;
      '';
    };
    virtualHosts."weini.klimacamp.eu" = {
      forceSSL = true;
      enableACME = true;
      locations."/".root = "/websites/klimacamp-weini/_site";
      locations."~* \\.(jpg|jpeg|png|gif|svg|webp|ico|css|js)$" = {
        root = "/websites/klimacamp-weini/_site";
        extraConfig = "expires 365d;";
      };
    };
    virtualHosts."www.weini.klimacamp.eu" = {
      enableACME = true;
      addSSL = true;
      locations."/".extraConfig = ''
        return 301 https://weini.klimacamp.eu$request_uri;
      '';
    };
    virtualHosts."www.lohwibleibt.de" = {
      forceSSL = true;
      enableACME = true;
      locations."/".root = "/klimacamp-augsburg/lohwibleibt";
      locations."~* \\.(jpg|jpeg|png|gif|svg|webp|ico|css|js)$" = {
        root = "/klimacamp-augsburg/lohwibleibt";
        extraConfig = "expires 365d;";
      };
    };
    virtualHosts."lohwibleibt.de" = {
      enableACME = true;
      addSSL = true;
      locations."/".extraConfig = ''
        return 301 https://www.lohwibleibt.de$request_uri;
      '';
    };
    virtualHosts."www.fff-dillingen.de" = {
      forceSSL = true;
      enableACME = true;
      locations."/".root = "/klimacamp-augsburg/fff-dillingen";
      locations."~* \\.(jpg|jpeg|png|gif|svg|webp|ico|css|js|webm|woff2)$" = {
        root = "/klimacamp-augsburg/fff-dillingen";
        extraConfig = "expires 365d;";
      };
    };
    virtualHosts."fff-dillingen.de" = {
      enableACME = true;
      addSSL = true;
      locations."/".extraConfig = ''
        return 301 https://www.fff-dillingen.de$request_uri;
      '';
    };
    virtualHosts."www.kidical-mass-augsburg.de" = {
      forceSSL = true;
      enableACME = true;
      locations."/".extraConfig = ''
        return 302 https://augsburg.adfc.de/artikel/kidicalmass;
      '';
    };
    virtualHosts."kidical-mass-augsburg.de" = {
      enableACME = true;
      addSSL = true;
      locations."/".extraConfig = ''
        return 301 https://www.kidical-mass-augsburg.de$request_uri;
      '';
    };
    virtualHosts."www.xn--klimacamp-nrdlingen-06b.de" = {
      forceSSL = true;
      enableACME = true;
      locations."/".extraConfig = ''
        return 302 https://linkstack.lgbt/@KlimacampN%C3%B6rdlingen$request_uri;
      '';
    };
    virtualHosts."xn--klimacamp-nrdlingen-06b.de" = {
      enableACME = true;
      addSSL = true;
      locations."/".extraConfig = ''
        return 302 https://linkstack.lgbt/@KlimacampN%C3%B6rdlingen$request_uri;
      '';
    };
    virtualHosts."halle.klimacamp.eu" = {
      forceSSL = true;
      enableACME = true;
      locations."/" = {
        proxyPass = "https://78.47.104.32";
        proxyWebsockets = true;
        extraConfig = ''
          proxy_set_header Host halle.klimacamp.eu;
        '';
      };
    };
    virtualHosts."www.halle.klimacamp.eu" = {
      enableACME = true;
      addSSL = true;
      locations."/".extraConfig = ''
        return 301 https://halle.klimacamp.eu$request_uri;
      '';
    };
    virtualHosts."klima.quasicoherent.io" = {
      forceSSL = true;
      enableACME = true;
      locations."/" = {
        proxyPass = "http://localhost:8432";
        proxyWebsockets = true;
        extraConfig = ''
          proxy_set_header Host localhost;
        '';
      };
    };
    virtualHosts."www.klima.quasicoherent.io" = {
      enableACME = true;
      addSSL = true;
      locations."/".extraConfig = ''
        return 301 https://klima.quasicoherent.io$request_uri;
      '';
    };
    virtualHosts."overleaf.antwerp-logic-adventures.be" = {
      forceSSL = true;
      enableACME = true;
      basicAuth = { antwerp = "antwerp"; };
      locations."/" = {
        proxyPass = "http://localhost:8021";
        proxyWebsockets = true;
        extraConfig = ''
          proxy_set_header Host localhost;
          proxy_set_header Authorization "";
        '';
      };
    };
#    virtualHosts."klima2.quasicoherent.io" = {
#      forceSSL = false;
#      enableACME = false;
#      extraConfig = builtins.readFile "/websites/klimakrise.conf";
#    };
    virtualHosts."analysis.quasicoherent.io" = {
      forceSSL = true;
      enableACME = true;
      locations."/".extraConfig = ''
        return 302 https://ana.mathe.sexy$request_uri;
      '';
    };
    virtualHosts."www.analysis.quasicoherent.io" = {
      enableACME = true;
      addSSL = true;
      locations."/".extraConfig = ''
        return 301 https://analysis.quasicoherent.io$request_uri;
      '';
    };
    virtualHosts."programmierkurs.quasicoherent.io" = {
      forceSSL = true;
      enableACME = true;
      locations."/" = {
        proxyPass = "https://algebra-und-zahlentheorie.gitlab.io/programmierkurs/";
      };
      locations."/_rebuild_univr".extraConfig = ''
        perl 'sub {
          mkdir "/run/univr-ipc/rebuild";
          $_[0]->send_http_header("text/plain");
          return OK;
        }';
      '';
    };
    virtualHosts."www.programmierkurs.quasicoherent.io" = {
      enableACME = true;
      addSSL = true;
      locations."/".extraConfig = ''
        return 301 https://programmierkurs.quasicoherent.io$request_uri;
      '';
    };
    virtualHosts."qf.quasicoherent.io" = {
      forceSSL = true;
      enableACME = true;
      locations."/" = {
        proxyPass = "https://iblech.gitlab.io/minicourse-quadratic-forms/";
      };
    };
    virtualHosts."www.qf.quasicoherent.io" = {
      enableACME = true;
      addSSL = true;
      locations."/".extraConfig = ''
        return 301 https://qf.quasicoherent.io$request_uri;
      '';
    };
    virtualHosts."ct.quasicoherent.io" = {
      forceSSL = true;
      enableACME = true;
      locations."/" = {
        proxyPass = "https://iblech.gitlab.io/minicourse-category-theory/";
      };
    };
    virtualHosts."www.ct.quasicoherent.io" = {
      enableACME = true;
      addSSL = true;
      locations."/".extraConfig = ''
        return 301 https://ct.quasicoherent.io$request_uri;
      '';
    };
    virtualHosts."rt.quasicoherent.io" = {
      forceSSL = true;
      enableACME = true;
      locations."/" = {
        proxyPass = "https://iblech-gitlab/minicourse-realizability-theory/";
        extraConfig = ''
          proxy_cache STATIC;
          proxy_set_header Host iblech.gitlab.io;
          proxy_redirect //minicourse-realizability-theory/ /;
          if ($request_uri ~* ".(jpg|jpeg|png|gif|svg|webp|ico|css|js|webm|woff2)") {
            expires 365d;
          }
          proxy_cache_bypass $http_cachepurge;
        '';
      };
    };
    virtualHosts."www.rt.quasicoherent.io" = {
      enableACME = true;
      addSSL = true;
      locations."/".extraConfig = ''
        return 301 https://rt.quasicoherent.io$request_uri;
      '';
    };
    virtualHosts."aff.quasicoherent.io" = {
      forceSSL = true;
      enableACME = true;
      locations."/" = {
        proxyPass = "https://iblech-gitlab/algebraic-function-fields/";
        #proxyPass = "https://speicherleck/iblech/stuff/.aff-draft/";
        extraConfig = ''
          proxy_cache STATIC;
          proxy_set_header Host iblech.gitlab.io;
          proxy_redirect //algebraic-function-fields/ /;
          #proxy_redirect //iblech/stuff/.aff-draft/ /;
          proxy_hide_header Upgrade;
          if ($request_uri ~* ".(jpg|jpeg|png|gif|svg|webp|ico|css|js|webm|woff2)") {
            expires 365d;
          }
          proxy_cache_bypass $http_cachepurge;
        '';
      };
    };
    virtualHosts."www.aff.quasicoherent.io" = {
      enableACME = true;
      addSSL = true;
      locations."/".extraConfig = ''
        return 301 https://aff.quasicoherent.io$request_uri;
      '';
    };
    virtualHosts."www.antwerp-logic-adventures.be" = {
      forceSSL = true;
      enableACME = true;
      locations."/" = {
        proxyPass = "https://iblech-gitlab/algebraic-function-fields/ala/";
        #proxyPass = "https://speicherleck/iblech/stuff/.aff-draft/ala/";
        extraConfig = ''
          proxy_cache STATIC;
          proxy_set_header Host iblech.gitlab.io;
          proxy_redirect //algebraic-function-fields/ala/ /;
          #proxy_redirect //iblech/stuff/.aff-draft/ala/ /;
          proxy_hide_header Upgrade;
          if ($request_uri ~* ".(jpg|jpeg|png|gif|svg|webp|ico|css|js|webm|woff2)") {
            expires 365d;
          }
          proxy_cache_bypass $http_cachepurge;
        '';
      };
    };
    virtualHosts."antwerp-logic-adventures.be" = {
      enableACME = true;
      addSSL = true;
      locations."/".extraConfig = ''
        return 301 https://www.antwerp-logic-adventures.be$request_uri;
      '';
    };
    virtualHosts."qed.quasicoherent.io" = {
      enableACME = true;
      addSSL = true;
      locations."/".extraConfig = ''
        return 302 https://rt.quasicoherent.io/qed$request_uri;
      '';
    };
    virtualHosts."www.qed.quasicoherent.io" = {
      enableACME = true;
      addSSL = true;
      locations."/".extraConfig = ''
        return 302 https://rt.quasicoherent.io/qed$request_uri;
      '';
    };
    virtualHosts."klimauni-augsburg.de" = {
      enableACME = true;
      addSSL = true;
      locations."/".extraConfig = ''
        return 301 https://www.klimauni-augsburg.de$request_uri;
      '';
    };
    virtualHosts."www.klimauni-augsburg.de" = {
      enableACME = true;
      forceSSL = true;
      locations."/".root = "/klimacamp-augsburg/klimauni-augsburg";
      locations."/".extraConfig = ''
        #http2_push_preload on;
        #add_header Link "</banner-pcs2.webp>; as=image; rel=preload, </jost-400.woff2>; rel=preload; as=font; crossorigin, </jost-700.woff2>; rel=preload; as=font; crossorigin";
        add_header Strict-Transport-Security $hsts_header;
      '';
      locations."~* \\.(jpg|jpeg|png|gif|svg|webp|ico|css|js|webm|woff2)$" = {
        root = "/klimacamp-augsburg/klimauni-augsburg";
        extraConfig = "expires 365d;";
      };
    };
    virtualHosts."whiteboard.quasicoherent.io" = {
      forceSSL = true;
      enableACME = true;
      locations."/" = {
        proxyPass = "https://whiteboard.speicherleck.de";
        proxyWebsockets = true;
        extraConfig = ''
          proxy_set_header Host whiteboard.speicherleck.de;
        '';
      };
    };
    virtualHosts."studentsforfuture-augsburg.de" = {
      enableACME = true;
      addSSL = true;
      locations."/".extraConfig = ''
        return 301 https://www.studentsforfuture-augsburg.de$request_uri;
      '';
    };
    virtualHosts."www.studentsforfuture-augsburg.de" = {
      enableACME = true;
      forceSSL = true;
      locations."/".root = "/klimacamp-augsburg/studentsforfuture-augsburg";
      locations."/".extraConfig = ''
        index index.html index-old.html;
        #http2_push_preload on;
        #add_header Link "</banner.webp>; as=image; rel=preload, </jost-400.woff2>; rel=preload; as=font; crossorigin, </jost-700.woff2>; rel=preload; as=font; crossorigin";
        add_header Strict-Transport-Security $hsts_header;
      '';
      locations."~* \\.(jpg|jpeg|png|gif|svg|webp|ico|css|js|webm|woff2)$" = {
        root = "/klimacamp-augsburg/studentsforfuture-augsburg";
        extraConfig = "expires 365d;";
      };
    };
    virtualHosts."calc.quasicoherent.io" = {
      forceSSL = true;
      enableACME = true;
      basicAuth = { klima = lib.removeSuffix "\n" (builtins.readFile "/root/ethercalc-pw"); };
      locations."/" = {
        proxyPass = "http://ethercalc.containers:8000";
        proxyWebsockets = true;
      };
    };
    virtualHosts."ulmer-uniwald-bleibt.de" = {
      enableACME = true;
      addSSL = true;
      locations."/".extraConfig = ''
        return 301 https://www.ulmer-uniwald-bleibt.de$request_uri;
      '';
    };
    virtualHosts."www.ulmer-uniwald-bleibt.de" = {
      enableACME = true;
      forceSSL = true;
      locations."/".root = "/websites/eselsberg";
      locations."/".extraConfig = ''
        #http2_push_preload on;
        #add_header Link "</images/banner-oct.webp>; as=image; rel=preload, </fonts/jost-400.woff2>; rel=preload; as=font; crossorigin, </fonts/jost-700.woff2>; rel=preload; as=font; crossorigin";
        add_header Strict-Transport-Security $hsts_header;
      '';
      locations."~* \\.(jpg|jpeg|png|gif|svg|webp|ico|css|js|webm|woff2)$" = {
        root = "/websites/eselsberg";
        extraConfig = "expires 365d;";
      };
      locations."~ /\\.git" = {
        extraConfig = "deny all;";
      };
    };
    virtualHosts."pissegate.de" = {
      enableACME = true;
      addSSL = true;
      locations."/".extraConfig = ''
        return 301 https://frauenstreikaux.blackblogs.org/2022/11/15/pissegate-pressemappe$request_uri;
      '';
    };
    virtualHosts."www.pissegate.de" = {
      enableACME = true;
      addSSL = true;
      locations."/".extraConfig = ''
        return 301 https://frauenstreikaux.blackblogs.org/2022/11/15/pissegate-pressemappe$request_uri;
      '';
    };
    virtualHosts."pimmelgate-sued.de" = {
      enableACME = true;
      addSSL = true;
      locations."/".extraConfig = ''
        return 301 https://www.xn--pimmelgate-sd-7ob.de$request_uri;
      '';
    };
    virtualHosts."www.pimmelgate-sued.de" = {
      enableACME = true;
      addSSL = true;
      locations."/".extraConfig = ''
        return 301 https://www.xn--pimmelgate-sd-7ob.de$request_uri;
      '';
    };
    virtualHosts."xn--pimmelgate-sd-7ob.de" = {
      enableACME = true;
      addSSL = true;
      locations."/".extraConfig = ''
        return 301 https://www.xn--pimmelgate-sd-7ob.de$request_uri;
      '';
    };
    virtualHosts."www.xn--pimmelgate-sd-7ob.de" = {
      enableACME = true;
      forceSSL = true;
      locations."/".root = "/klimacamp-augsburg/pimmelgate-sued";
      locations."/".extraConfig = ''
        #http2_push_preload on;
        #add_header Link "</banner.jpeg>; as=image; rel=preload, </fonts/jost-400.woff2>; rel=preload; as=font; crossorigin, </fonts/jost-700.woff2>; rel=preload; as=font; crossorigin";
        add_header Strict-Transport-Security $hsts_header;
        autoindex on;
      '';
      locations."~* \\.(jpg|jpeg|png|gif|svg|webp|ico|css|js|webm|woff2|pdf)$" = {
        root = "/klimacamp-augsburg/pimmelgate-sued";
        extraConfig = "expires 365d;";
      };
    };
#    upstreams.laurenz = {
#      servers = { "verkehrswende.dawn-privacy.org:443" = {}; };
#      extraConfig = ''
#        keepalive 10;
#      '';
#    };
#    virtualHosts."www.verkehrswende-augsburg.de" = {
#      forceSSL = true;
#      enableACME = true;
#      locations."/" = {
#        proxyPass = "https://laurenz/";
#        proxyWebsockets = true;
#        extraConfig = ''
#          proxy_cache STATIC;
#          proxy_set_header Host verkehrswende.dawn-privacy.org;
#          proxy_cache_bypass $http_cachepurge;
#          proxy_hide_header Upgrade;
#        '';
#        # basicAuth = { klima = lib.removeSuffix "\n" (builtins.readFile "/root/ethercalc-pw"); };
#      };
#      extraConfig = ''
#        access_log /var/log/nginx/verkehrswende-augsburg.log vhost_combined;
#      '';
#    };
#    virtualHosts."verkehrswende-augsburg.de" = {
#      enableACME = true;
#      addSSL = true;
#      locations."/".extraConfig = ''
#        return 301 https://www.verkehrswende-augsburg.de$request_uri;
#      '';
#    };
    virtualHosts."www.bobinger-auwald-bleibt.de" = {
      forceSSL = true;
      enableACME = true;
      locations."/".root = "/klimacamp-augsburg/bobinger-auwald-bleibt.de";
      locations."~* \\.(jpg|jpeg|png|gif|svg|webp|ico|css|js|woff2)$" = {
        root = "/klimacamp-augsburg/bobinger-auwald-bleibt.de";
        extraConfig = "expires 365d;";
      };
      locations."/".extraConfig = ''
        #http2_push_preload on;
        #add_header Link "</banner.webp>; as=image; rel=preload, </fonts/jost-400.woff2>; rel=preload; as=font; crossorigin, </fonts/jost-700.woff2>; rel=preload; as=font; crossorigin";
        add_header Strict-Transport-Security $hsts_header;
        autoindex on;
      '';
    };
    virtualHosts."bobinger-auwald-bleibt.de" = {
      enableACME = true;
      addSSL = true;
      locations."/".extraConfig = ''
        return 301 https://www.bobinger-auwald-bleibt.de$request_uri;
      '';
    };
#    virtualHosts."www.augsburg.klimacamp.eu" = {
#      enableACME = true;
#      addSSL = true;
#      locations."/".extraConfig = ''
#        return 301 https://www.klimacamp-augsburg.de$request_uri;
#      '';
#    };
    virtualHosts."augsburg.klimacamp.eu" = {
      enableACME = true;
      addSSL = true;
      locations."/".extraConfig = ''
        return 301 https://www.klimacamp-augsburg.de$request_uri;
      '';
    };
    virtualHosts."klimacamp-augsburg.de" = {
      enableACME = true;
      addSSL = true;
      locations."/".extraConfig = ''
        return 301 https://www.klimacamp-augsburg.de$request_uri;
      '';
    };
    upstreams.klimacamp-augsburg-gitlab = {
      servers = { "baldrian.gitlab.io:443" = { "max_fails" = "0"; }; };
      extraConfig = ''
        keepalive 10;
      '';
    };
    upstreams.karlsruhe-gitlab = {
      servers = { "platanen-ka.gitlab.io:443" = { "max_fails" = "0"; }; };
      extraConfig = ''
        keepalive 10;
      '';
    };
    upstreams.autofreitag-gitlab = {
      servers = { "auto-frei-tag.gitlab.io:443" = { "max_fails" = "0"; }; };
      extraConfig = ''
        keepalive 10;
      '';
    };
    upstreams.klimacamp-ravensburg-gitlab = {
      servers = { "iblech.gitlab.io:443" = { "max_fails" = "0"; }; };
      extraConfig = ''
        keepalive 10;
      '';
    };
    upstreams.alti-gitlab = {
      servers = { "alti.bahnvorhersage.de:443" = { "max_fails" = "0"; }; };
      extraConfig = ''
        keepalive 10;
      '';
    };
    upstreams.iblech-gitlab = {
      servers = { "iblech.gitlab.io:443" = { "max_fails" = "0"; }; };
      extraConfig = ''
        keepalive 10;
      '';
    };
    upstreams.speicherleck = {
      servers = { "www.speicherleck.de:443" = { "max_fails" = "0"; }; };
      extraConfig = ''
        keepalive 10;
      '';
    };
    upstreams.jesus-klimaaktivist-gitlab = {
      servers = { "jesus-klimaaktivist.gitlab.io:443" = { "max_fails" = "0"; }; };
      extraConfig = ''
        keepalive 10;
      '';
    };
    upstreams.endfossilaugsburg-gitlab = {
      servers = { "mctoel.gitlab.io:443" = { "max_fails" = "0"; }; };
      extraConfig = ''
        keepalive 10;
      '';
    };
    upstreams.kollektivr-gitlab = {
      servers = { "louaux.gitlab.io:443" = { "max_fails" = "0"; }; };
      extraConfig = ''
        keepalive 10;
      '';
    };
    virtualHosts."webalizer.quasicoherent.io" = {
      enableACME = true;
      forceSSL = true;
      locations."/" = {
        root = "/webalizer";
      };
    };
    virtualHosts."www.klimacamp-augsburg.de" = {
      enableACME = true;
      forceSSL = true;
      locations."/" = {
        #proxyPass = "https://klimacamp-augsburg-gitlab/klimacamp-augsburg/";
        root = "/klimacamp-augsburg/_site";
#        extraConfig = ''
#          proxy_cache STATIC;
#          proxy_set_header Host baldrian.gitlab.io;
#          proxy_redirect //baldrian.gitlab.io/klimacamp-augsburg/ /;
#          if ($request_uri ~* ".(jpg|jpeg|png|gif|svg|webp|ico|css|js|webm|woff2)$") {
#            expires 365d;
#          }
#          proxy_cache_bypass $http_cachepurge;
#        '';
      };
      locations."~* \\.(jpg|jpeg|png|gif|svg|webp|ico|css|js|webm|woff2)$" = {
        root = "/klimacamp-augsburg/_site";
        extraConfig = "expires 365d;";
      };
      locations."/demo-der-bäume".return = "302 https://www.klimacamp-augsburg.de/informationen/artikel/baeume";
      locations."/demo-der-baeume".return = "302 https://www.klimacamp-augsburg.de/informationen/artikel/baeume";
      locations."/_rebuild".extraConfig = ''
        perl 'sub {
          mkdir "/run/klimacamp-augsburg-ipc/rebuild";
          $_[0]->send_http_header("text/plain");
          return OK;
        }';
      '';
      locations."/subscribe/" = {
        proxyPass = "https://www.speicherleck.de/cgi-bin/subscribe-kca.pl";
        extraConfig = ''
          proxy_hide_header Upgrade;
        '';
      };
      extraConfig = ''
        access_log /var/log/nginx/klimacamp-augsburg.log vhost_combined;
      '';
    };
    virtualHosts."www.kollektivr.net" = {
      enableACME = true;
      forceSSL = true;
      locations."/" = {
        proxyPass = "https://kollektivr-gitlab/kollektivr/";
        extraConfig = ''
          proxy_cache STATIC;
          proxy_set_header Host louaux.gitlab.io;
          proxy_redirect //kollektivr/ /;
          if ($request_uri ~* ".(jpg|jpeg|png|gif|svg|webp|ico|css|js|webm|woff2)") {
            expires 365d;
          }
          proxy_cache_bypass $http_cachepurge;
        '';
      };
      extraConfig = ''
        access_log /var/log/nginx/kollektivr.log vhost_combined;
      '';
    };
    virtualHosts."kollektivr.net" = {
      enableACME = true;
      addSSL = true;
      locations."/".extraConfig = ''
        return 301 https://www.kollektivr.net$request_uri;
      '';
    };
    virtualHosts."www.qualle-augsburg.de" = {
      enableACME = true;
      forceSSL = true;
      locations."/" = {
        proxyPass = "https://kollektivr-gitlab/qualle-augsburg/";
        extraConfig = ''
          proxy_cache STATIC;
          proxy_set_header Host louaux.gitlab.io;
          proxy_redirect //qualle-augsburg/ /;
          if ($request_uri ~* ".(jpg|jpeg|png|gif|svg|webp|ico|css|js|webm|woff2)") {
            expires 365d;
          }
          proxy_cache_bypass $http_cachepurge;
        '';
      };
      extraConfig = ''
        access_log /var/log/nginx/qualle-augsburg.log vhost_combined;
      '';
    };
    virtualHosts."qualle-augsburg.de" = {
      enableACME = true;
      addSSL = true;
      locations."/".extraConfig = ''
        return 301 https://www.qualle-augsburg.de$request_uri;
      '';
    };
#    virtualHosts."www.greenoffice-augsburg.de" = {
#      enableACME = true;
#      forceSSL = true;
#      locations."/" = {
#        proxyPass = "https://kollektivr-gitlab/greenoffice-augsburg/";
#        extraConfig = ''
#          proxy_cache STATIC;
#          proxy_set_header Host louaux.gitlab.io;
#          proxy_redirect //greenoffice-augsburg/ /;
#          if ($request_uri ~* ".(jpg|jpeg|png|gif|svg|webp|ico|css|js|webm|woff2)") {
#            expires 365d;
#          }
#          proxy_cache_bypass $http_cachepurge;
#        '';
#      };
#      extraConfig = ''
#        access_log /var/log/nginx/greenoffice-augsburg.log vhost_combined;
#      '';
#    };
#    virtualHosts."greenoffice-augsburg.de" = {
#      enableACME = true;
#      addSSL = true;
#      locations."/".extraConfig = ''
#        return 301 https://www.greenoffice-augsburg.de$request_uri;
#      '';
#    };
    virtualHosts."www.endfossilaugsburg.de" = {
      enableACME = true;
      forceSSL = true;
      locations."/" = {
        proxyPass = "https://endfossilaugsburg-gitlab/end-fossil-augsburg-website/";
        extraConfig = ''
          proxy_cache STATIC;
          proxy_set_header Host mctoel.gitlab.io;
          proxy_redirect //end-fossil-augsburg-website/ /;
          if ($request_uri ~* ".(jpg|jpeg|png|gif|svg|webp|ico|css|js|webm|woff2)") {
            expires 365d;
          }
          proxy_cache_bypass $http_cachepurge;
        '';
      };
      extraConfig = ''
        access_log /var/log/nginx/endfossilaugsburg.log vhost_combined;
      '';
    };
    virtualHosts."endfossilaugsburg.de" = {
      enableACME = true;
      addSSL = true;
      locations."/".extraConfig = ''
        return 301 https://www.endfossilaugsburg.de$request_uri;
      '';
    };
    virtualHosts."www.karlsruher-platanen-bleiben.de" = {
      enableACME = true;
      forceSSL = true;
      locations."/" = {
        #basicAuth = { klima = "gerechtigkeit"; };
        proxyPass = "https://karlsruhe-gitlab/website/";
        extraConfig = ''
          proxy_cache STATIC;
          proxy_set_header Host platanen-ka.gitlab.io;
          proxy_redirect //website/ /;
          if ($request_uri ~* ".(jpg|jpeg|png|gif|svg|webp|ico|css|js|webm|woff2)") {
            expires 365d;
          }
          proxy_cache_bypass $http_cachepurge;
        '';
      };
      locations."/subscribe/" = {
        proxyPass = "https://www.speicherleck.de/cgi-bin/subscribe-karlsruhe.pl";
        extraConfig = ''
          proxy_hide_header Upgrade;
        '';
      };
      extraConfig = ''
        access_log /var/log/nginx/karlsruhe.log vhost_combined;
      '';
    };
    virtualHosts."karlsruher-platanen-bleiben.de" = {
      enableACME = true;
      addSSL = true;
      locations."/".extraConfig = ''
        return 301 https://www.karlsruher-platanen-bleiben.de$request_uri;
      '';
    };
    virtualHosts."ravensburg.klimacamp.eu" = {
      enableACME = true;
      forceSSL = true;
      locations."/" = {
        proxyPass = "https://alti-gitlab/";
        extraConfig = ''
          proxy_cache STATIC;
          proxy_set_header Host alti.bahnvorhersage.de;
          proxy_redirect //alti.bahnvorhersage.de/ /;
          if ($request_uri ~* ".(jpg|jpeg|png|gif|svg|webp|ico|css|js|webm|woff2)") {
            expires 365d;
          }
          proxy_cache_bypass $http_cachepurge;
        '';
      };
      locations."/whatsapp".extraConfig = ''
        return 301 https://chat.whatsapp.com/Ir1uR6O54FyAYML3CBgEQP;
      '';
    };
    virtualHosts."classic-ravensburg.klimacamp-augsburg.de" = {
      enableACME = true;
      forceSSL = true;
      locations."/" = {
        proxyPass = "https://klimacamp-ravensburg-gitlab/klimacamp-ravensburg/";
        extraConfig = ''
          proxy_cache STATIC;
          proxy_set_header Host iblech.gitlab.io;
          proxy_redirect //iblech.gitlab.io/klimacamp-ravensburg/ /;
          if ($request_uri ~* ".(jpg|jpeg|png|gif|svg|webp|ico|css|js|webm|woff2)") {
            expires 365d;
          }
          proxy_cache_bypass $http_cachepurge;
        '';
      };
    };
    upstreams."ncatlab.org" = {
      servers = { "10.0.1.2:443" = { "max_fails" = "0"; }; };
      extraConfig = ''
        keepalive 10;
      '';
    };
    upstreams."nforum.ncatlab.org" = {
      servers = { "10.0.1.2:443" = { "max_fails" = "0"; }; };
      extraConfig = ''
        keepalive 10;
      '';
    };
    virtualHosts."nlab-mirror.quasicoherent.io" = {
      forceSSL = true;
      enableACME = true;
      locations."/" = {
        basicAuth = { nlab = "nlab"; };
        proxyPass = "https://ncatlab.org/";
        proxyWebsockets = true;
        extraConfig = ''
          proxy_busy_buffers_size   512k;
          proxy_buffers   4 512k;
          proxy_buffer_size   256k;
          proxy_cache STATIC;
          proxy_set_header Host ncatlab.org;
          proxy_cache_bypass $http_cachepurge;
          proxy_hide_header Upgrade;
          proxy_redirect https://www.ncatlab.org/ https://nlab-mirror.quasicoherent.io/;
          proxy_redirect https://ncatlab.org/ https://nlab-mirror.quasicoherent.io/;
          proxy_redirect https://nforum.ncatlab.org/ https://nforum-mirror.quasicoherent.io/;
          proxy_redirect https://temporary-nlab-mirror.quasicoherent.io/ https://nlab-mirror.quasicoherent.io/;
          proxy_redirect https://temporary-nforum-mirror.quasicoherent.io/ https://nforum-mirror.quasicoherent.io/;
          proxy_redirect http://www.ncatlab.org/ https://nlab-mirror.quasicoherent.io/;
          proxy_redirect http://ncatlab.org/ https://nlab-mirror.quasicoherent.io/;
          proxy_redirect http://nforum.ncatlab.org/ https://nforum-mirror.quasicoherent.io/;
          proxy_redirect http://temporary-nlab-mirror.quasicoherent.io/ https://nlab-mirror.quasicoherent.io/;
          proxy_redirect http://temporary-nforum-mirror.quasicoherent.io/ https://nforum-mirror.quasicoherent.io/;
          sub_filter "https://www.ncatlab.org" "https://nlab-mirror.quasicoherent.io";
          sub_filter "https://ncatlab.org" "https://nlab-mirror.quasicoherent.io";
          sub_filter "https://nforum.ncatlab.org" "https://nforum-mirror.quasicoherent.io";
          sub_filter "https://temporary-nlab-mirror.quasicoherent.io" "https://nlab-mirror.quasicoherent.io";
          sub_filter "https://temporary-nforum-mirror.quasicoherent.io" "https://nforum-mirror.quasicoherent.io";
          sub_filter "http://www.ncatlab.org" "https://nlab-mirror.quasicoherent.io";
          sub_filter "http://ncatlab.org" "https://nlab-mirror.quasicoherent.io";
          sub_filter "http://nforum.ncatlab.org" "https://nforum-mirror.quasicoherent.io";
          sub_filter "temporary-nlab-mirror.quasicoherent.io" "nlab-mirror.quasicoherent.io";
          sub_filter "temporary-nforum-mirror.quasicoherent.io" "nforum-mirror.quasicoherent.io";
          sub_filter_types *;
          sub_filter_once off;
        '';
      };
    };
    virtualHosts."nforum-mirror.quasicoherent.io" = {
      forceSSL = true;
      enableACME = true;
      locations."/" = {
        basicAuth = { nlab = "nlab"; };
        proxyPass = "https://nforum.ncatlab.org/";
        proxyWebsockets = true;
        extraConfig = ''
          proxy_cache STATIC;
          proxy_set_header Host nforum.ncatlab.org;
          proxy_cache_bypass $http_cachepurge;
          proxy_hide_header Upgrade;
          proxy_redirect https://www.ncatlab.org/ https://nlab-mirror.quasicoherent.io/;
          proxy_redirect https://ncatlab.org/ https://nlab-mirror.quasicoherent.io/;
          proxy_redirect https://nforum.ncatlab.org/ https://nforum-mirror.quasicoherent.io/;
          proxy_redirect https://temporary-nlab-mirror.quasicoherent.io/ https://nlab-mirror.quasicoherent.io/;
          proxy_redirect https://temporary-nforum-mirror.quasicoherent.io/ https://nforum-mirror.quasicoherent.io/;
          proxy_redirect http://www.ncatlab.org/ https://nlab-mirror.quasicoherent.io/;
          proxy_redirect http://ncatlab.org/ https://nlab-mirror.quasicoherent.io/;
          proxy_redirect http://nforum.ncatlab.org/ https://nforum-mirror.quasicoherent.io/;
          proxy_redirect http://temporary-nlab-mirror.quasicoherent.io/ https://nlab-mirror.quasicoherent.io/;
          proxy_redirect http://temporary-nforum-mirror.quasicoherent.io/ https://nforum-mirror.quasicoherent.io/;
          sub_filter "https://www.ncatlab.org" "https://nlab-mirror.quasicoherent.io";
          sub_filter "https://ncatlab.org" "https://nlab-mirror.quasicoherent.io";
          sub_filter "https://nforum.ncatlab.org" "https://nforum-mirror.quasicoherent.io";
          sub_filter "https://temporary-nlab-mirror.quasicoherent.io" "https://nlab-mirror.quasicoherent.io";
          sub_filter "https://temporary-nforum-mirror.quasicoherent.io" "https://nforum-mirror.quasicoherent.io";
          sub_filter "http://www.ncatlab.org" "https://nlab-mirror.quasicoherent.io";
          sub_filter "http://ncatlab.org" "https://nlab-mirror.quasicoherent.io";
          sub_filter "http://nforum.ncatlab.org" "https://nforum-mirror.quasicoherent.io";
          sub_filter "temporary-nlab-mirror.quasicoherent.io" "nlab-mirror.quasicoherent.io";
          sub_filter "temporary-nforum-mirror.quasicoherent.io" "nforum-mirror.quasicoherent.io";
          sub_filter_types *;
          sub_filter_once off;
        '';
      };
    };
    virtualHosts."pacman.logicverona.it" = {
      forceSSL = true;
      enableACME = true;
      locations."/".root = "/websites/pacman.logicverona.it/wwwroot";
    };
    virtualHosts."www.pacman.logicverona.it" = {
      enableACME = true;
      addSSL = true;
      locations."/".extraConfig = ''
        return 301 https://pacman.logicverona.it$request_uri;
      '';
    };
    virtualHosts."www.auto-frei-tag.de" = {
      enableACME = true;
      forceSSL = true;
      locations."/" = {
        proxyPass = "https://autofreitag-gitlab/website/";
        extraConfig = ''
          proxy_cache STATIC;
          proxy_set_header Host auto-frei-tag.gitlab.io;
          proxy_redirect //website/ /;
          if ($request_uri ~* ".(jpg|jpeg|png|gif|svg|webp|ico|css|js|webm|woff2)") {
            expires 365d;
          }
          proxy_cache_bypass $http_cachepurge;
        '';
      };
      locations."/subscribe/" = {
        proxyPass = "https://www.speicherleck.de/cgi-bin/subscribe-autofreitag.pl";
        extraConfig = ''
          proxy_hide_header Upgrade;
        '';
      };
      extraConfig = ''
        access_log /var/log/nginx/autofreitag.log vhost_combined;
      '';
    };
    virtualHosts."auto-frei-tag.de" = {
      enableACME = true;
      addSSL = true;
      locations."/".extraConfig = ''
        return 301 https://www.auto-frei-tag.de$request_uri;
      '';
    };
    virtualHosts."autofreitag.de" = {
      enableACME = true;
      addSSL = true;
      locations."/".extraConfig = ''
        return 301 https://www.auto-frei-tag.de$request_uri;
      '';
    };
    virtualHosts."www.autofreitag.de" = {
      enableACME = true;
      addSSL = true;
      locations."/".extraConfig = ''
        return 301 https://www.auto-frei-tag.de$request_uri;
      '';
    };
    virtualHosts."www.radlnacht-augsburg.de" = {
      forceSSL = true;
      enableACME = true;
      locations."/".root = "/klimacamp-augsburg/radlnacht-augsburg";
      locations."~* \\.(jpg|jpeg|png|gif|svg|webp|ico|css|js)$" = {
        root = "/klimacamp-augsburg/radlnacht-augsburg";
        extraConfig = "expires 365d;";
      };
    };
    virtualHosts."radlnacht-augsburg.de" = {
      enableACME = true;
      addSSL = true;
      locations."/".extraConfig = ''
        return 301 https://www.radlnacht-augsburg.de$request_uri;
      '';
    };
    virtualHosts."www.xn--wre-jesus-klimaaktivist-v7b.de" = {
      forceSSL = true;
      enableACME = true;
      locations."/" = {
        proxyPass = "https://jesus-klimaaktivist-gitlab/website/";
        extraConfig = ''
          proxy_cache STATIC;
          proxy_set_header Host jesus-klimaaktivist.gitlab.io;
          proxy_redirect //website/ /;
          if ($request_uri ~* ".(jpg|jpeg|png|gif|svg|webp|ico|css|js|webm|woff2)") {
            expires 365d;
          }
          proxy_cache_bypass $http_cachepurge;
        '';
      };
    };
    virtualHosts."xn--wre-jesus-klimaaktivist-v7b.de" = {
      enableACME = true;
      addSSL = true;
      locations."/".extraConfig = ''
        return 301 https://www.xn--wre-jesus-klimaaktivist-v7b.de$request_uri;
      '';
    };
  };

  security.acme.defaults.email = "iblech@speicherleck.de";
  security.acme.acceptTerms = true;

  services.sslh = {
    enable = false;
    settings.transparent = true;
    settings.timeout = 3;
    settings.protocols =
      [ { host = "localhost"; name = "ssh"; port = "223"; service = "ssh"; }
        { host = "localhost"; name = "tls"; port = "4443"; }
      ];
  };

  networking.extraHosts = ''
    173.249.60.202 speicherleck.de
    173.249.60.202 www.speicherleck.de
    173.249.60.202 whiteboard.speicherleck.de
    167.86.79.68   ncatlab.org www.ncatlab.org nforum.ncatlab.org
  '';

  documentation.doc.enable = false;

  networking.nat.enable = true;
  networking.nat.externalInterface = "ens18";
  networking.nat.internalInterfaces = [ "wg0" ];

  networking.wireguard.interfaces = {
    wg0 = {
      ips = [ "10.0.0.1/24" ];
      listenPort = 51820;
      postSetup = ''
        ${pkgs.iptables}/bin/iptables -t nat -A POSTROUTING -s 10.0.0.0/24 -o ens18 -j MASQUERADE
      '';
      postShutdown = ''
        ${pkgs.iptables}/bin/iptables -t nat -D POSTROUTING -s 10.0.0.0/24 -o ens18 -j MASQUERADE
      '';
      privateKeyFile = "/root/wg-private";
      peers = [
        { publicKey = "ASjiyOzPrn6HVG1WTpMYJMVSQNpZ8tnOxr/lCJTD4GA=";
          allowedIPs = [ "10.0.0.2/32" ];
        }
      ];
    };
  };

  systemd.services.sshtunnel = {
    serviceConfig = {
      ExecStart = "${pkgs.openssh}/bin/ssh -o ServerAliveInterval=15 -T -Llocalhost:8432:localhost:8432 tunnel@speicherleck.de";
      User = "root";
      Restart = "always";
      RestartSec = "10";
    };
    wantedBy = [ "multi-user.target" ];
  };

  systemd.services.sshtunnel2 = {
    serviceConfig = {
      ExecStart = "${pkgs.openssh}/bin/ssh -o ServerAliveInterval=15 -T -Llocalhost:8021:localhost:8021 -p 223 antwerp@tor.quasicoherent.io";
      User = "antwerp";
      Restart = "always";
      RestartSec = "10";
    };
    wantedBy = [ "multi-user.target" ];
  };

  services.speechd.enable = false;

  users.users.antwerp = { isNormalUser = true; description = "Antwerp"; home = "/antwerp"; uid = 20003; extraGroups = [ "docker" ]; };
#  containers.antwerp = {
#    config =
#      { config, pkgs, ... }:
#      {
#        system.stateVersion = "${config.system.nixos.release}";
#        users.users.antwerp = { isNormalUser = true; description = "Antwerp"; home = "/home/antwerp"; uid = 20003; extraGroups = [ "docker" ]; };
#        users.groups.antwerp = { gid = 20003; members = [ "antwerp" ]; };
#        networking.firewall.enable = false;
#        virtualisation.docker.enable = true;
#        virtualisation.podman.enable = true;
#        networking.nameservers = [ "8.8.8.8" ];
#        environment.systemPackages = with pkgs; [
#          docker-compose vim screen
#        ];
#      };
#    ephemeral = false;
#    autoStart = false;
#    privateNetwork = true;
#    hostAddress = "192.168.3.1";
#    localAddress = "192.168.3.2";
#    bindMounts = { "/home/antwerp" = { hostPath = "/antwerp"; isReadOnly = false; }; };
#    extraFlags = [ "--system-call-filter=@keyring --system-call-filter=add_key --system-call-filter=bpf --system-call-filter=keyctl" ];
#  };

  #virtualisation.docker.enable = true;
  #virtualisation.podman.enable = true;
}
