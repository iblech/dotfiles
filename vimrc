sy on
set bg=dark smarttab ai sw=2
set mouse=
set ttymouse=
set ic
au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g`\"" | endif
set tw=79

set t_Co=256
set nojoinspaces

" https://superuser.com/a/132025
set comments-=mb:*
set comments+=fb:*

digraph bN 8469
digraph bT 120139
digraph bR 8477
digraph \|> 8614
digraph TT 8868
digraph \|- 8866

set nofsync
set swapsync=

if &term =~ "screen"
  let &t_BE = "\e[?2004h"
  let &t_BD = "\e[?2004l"
  exec "set t_PS=\e[200~"
  exec "set t_PE=\e[201~"
endif

set expandtab
