{config, pkgs, ...}:

{
  imports = [
    <nixpkgs/nixos/modules/installer/cd-dvd/installation-cd-minimal.nix>
    <nixpkgs/nixos/modules/installer/cd-dvd/channel.nix>
  ];

  services.journald.extraConfig = "Storage=volatile";
  boot.kernel.sysctl = { "vm.dirty_writeback_centisecs" = 6000; };
  boot.kernelPackages = pkgs.linuxPackages_4_17;

  boot.initrd.luks.devices.crypted.device = "/dev/disk/by-uuid/6487ed51-10b4-611a-6263-3145c06e0e68";
  fileSystems."/home/iblech" = {
    device = "/dev/mapper/crypted";
    fsType = "ext4";
    options = [ "noatime", "commit=20" ];
  };

  networking.hostName = "arolla";
  networking.firewall.enable = false;
  networking.supplicant = {
    "wlp0s26f7u2" = {
      configFile.path = "/home/iblech/wpa_supplicant.conf";
      userControlled.enable = true;
      userControlled.group = "iblech";
    };
  };

  i18n = {
    consoleKeyMap = "de";
    defaultLocale = "de_DE.UTF-8";
  };

  time.timeZone = "Europe/Berlin";
  services.timesyncd.servers = [ "ntp1.uni-augsburg.de" "0.nixos.pool.ntp.org" ];

  hardware.pulseaudio.enable = true;

  environment.systemPackages = with pkgs; [
    wget bc vim manpages screen file mosh psmisc unzip pciutils usbutils parallel lsof
    git tig
    nmap iptables ethtool dnsutils wireshark
    wpa_supplicant
    xorg.xgamma xdotool xorg.xkill xcalib redshift xrandr-invert-colors xorg.xwininfo
    xorg.xev xorg.xmodmap xsel xvkbd
    rfkill
    (texlive.combine {
      inherit (texlive) scheme-medium biblatex logreq xstring csquotes tabto-ltx
      soul xypic libertine chngcntr multirow bussproofs breakurl mweights fontaxes;
    })
    biber
    mupdf djview
    feh
    ghc
    gnuplot
    ruby
    python
    gnumake pandoc
    dillo elinks lynx
    imagemagick
    irssi
    tightvnc
    gifsicle libav
    alsaUtils pavucontrol
    sshfs

    # networkmanagerapplet lxpanel
    # powertop
    chromium firefoxWrapper
    gimp inkscape
    mpv
    xawtv xorg.xeyes
    xaos
    kobodeluxe
    haskellPackages.ghcid haskellPackages.cabal2nix haskellPackages.cabal-install
    # libreoffice-fresh
    # okular xorg.libxcb libsForQt5.phonon-backend-vlc gifsicle libav
  ];

  fonts.fonts = with pkgs; [
    hack-font ubuntu_font_family
  ];

  environment.variables = {
    EDITOR = "vim";
  };

  users.groups = { iblech = { gid = 1000; }; };
  users.users.iblech = {
    isNormalUser = true;
    description = "Ingo Blechschmidt";
    createHome = true;
    home = "/home/iblech";
    uid = 1000;
    group = "iblech";
    initialHashedPassword = "$6$A3YScMxrm$D/dP3mwSytkV8eDiIQNDAyh2FHebnn.lj7ZeDCbfst.baLglM10US7rXv6HLY7UHgl/kGc7Y4uiYPWqzy0DGW1";
  };

  services.openssh.enable = true;

  services.xserver = {
    enable = true;
    layout = "de";
    xkbVariant = "nodeadkeys";
    # videoDrivers = [ "modesetting" ];
    windowManager.xmonad = {
      enable = true;
      enableContribAndExtras = true;
    };
    windowManager.default = "xmonad";
    desktopManager.xterm.enable = true;
    displayManager = {
      slim = {
        enable = true;
        defaultUser = "iblech";
        autoLogin = true;
      };
    };
    libinput = {
      enable = true;
      middleEmulation = true;
    };
  };

  services.tor.enable = true;
  services.tor.client.enable = true;
  services.tor.client.privoxy.enable = false;
  services.privoxy.enable = true;
  services.privoxy.extraConfig = ''
    forward-socks4a / 127.0.0.1:9063 .
    # ;-(
    forward google.* .
    forward *.google.* .
    forward *.google.*.* .
    forward wifionice.de .
    forward iceportal.de .
     forward *.wifionice.de .
    forward *.iceportal.de .
    toggle 1
    enable-remote-toggle 0
    enable-edit-actions 0
    enable-remote-http-toggle 0
  '';
}
