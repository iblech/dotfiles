{ config, pkgs, ... }:

{
  system.stateVersion = "22.11";

  nix.settings.sandbox = true;
  nix.extraOptions = ''
    experimental-features = nix-command flakes
  '';

  imports =
    [ ./hardware-configuration.nix
    ];

  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/sda";

  boot.tmp.useTmpfs = true;

  boot.kernel.sysctl = {
    "vm.dirty_writeback_centisecs" = 6000;
    "fs.inotify.max_user_watches" = 163840;
    "fs.inotify.max_user_instances" = 2048;
    "fs.file-max" = 655360;
  };
  boot.kernelPackages = pkgs.linuxPackages_latest;
  boot.kernelParams = [ "cgroup_enable=memory" "swapaccount=1" ];
  services.journald.extraConfig = "SystemMaxUse=200M";

  boot.initrd.network = {
    enable = true;
    ssh = {
      enable = true;
      port = 2222;
      authorizedKeys = [
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCI5OnDnwv7cMIazGuIzDIam6oFM9HihieheJG8i3Zq2dqvuIfVhPj3RScXvfZ1BezWkHmrJtY6TxwkJ+y60/0d1h3xS3szrl9tqRpzxcFsd6i4boG2rBvWb6sEAvSc6NKQXx1v77lG0M2a90iXnh7A4QqfxB3/AbgFHK4dC08Ru6YDfHZKgddKh2ccgxyFO9txVRsjQCr6Tl+qa6VJpIRTJzdmBJFNHSciBCuX97VXhcxHj477ISm6LM90JW6RPhzTx5aOGYr/6qHYDWjEX76FHCajH+ZkIC4lb2gYEufkTu7wQFjt6JDY4hinZGSpbbZykzjV8zCGwLzwabvZRXhM8fdKWooOgALiP7LtXblLwZPfGAtpgJ7IUqmibTTzrDp+A/gk5vqyW7HjshN5OoHs9PaG/NzC/K//QbqEsGbNskHNjshfRXa+F86PptaaLdkDZXneN3Wnb7KECkC2DO+MN6EOJbR0xX599Te6WeFvhZ7ItHKH3g+8sFW0RQ4kVaE= user@arolla"
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDgHGlFbHg6aTLDMO806T7SQ7dXUe2a245ogouNJM54GQ+LscOVIAbttFM9zAIUjzxX1e6JTRdH/EDBu6bO+/EeTokB0hLJJV/Ht5W2zAWS0XWXoXGDdbWFnun+SPEQj94T44F7sVyXajRea0nSTK/XIwOB2CSFJRob+yIYj6LSJ9KwBWf9LHmiMqqNGvCVmgb1DLXZMrqWxpV4MNIzLGO0FxL2IxLdjEECHrpLRLBY67DBM6Kc5jp2x+HsNrPb0e9mB35QnpwKV+J8EkqswQoX/DxXNvwAfm8LPLPTSXC/5Qsh59IoY6pmwGA5+kLeIkaGXPfQ1ilYWasp1s13H6yJ iblech@quasicoherent.io"
      ];
      hostKeys = [ /etc/nixos/initrd_ecdsa_key ];
    };
  };

  networking.hostName = "quasilila";

  time.timeZone = "Europe/Amsterdam";
  services.timesyncd.servers = [ "0.nixos.pool.ntp.org" ];

  i18n = {
    defaultLocale = "en_US.UTF-8";
    extraLocaleSettings.LC_TIME = "C.UTF-8";
  };

  services.openssh.enable = true;
  services.openssh.ports = [ 222 ];
  services.openssh.settings.PermitRootLogin = "yes";
  networking.firewall.enable = false;

  environment.systemPackages = with pkgs; [
    wget bc vim man-pages screen file mosh unzip parallel binutils-unwrapped
    perl
    psmisc pciutils usbutils lsof powertop
    git tig
    nmap iptables ethtool dnsutils arp-scan socat
    mutt procmail fetchmail
    sshfs
    lynx elinks
    cryptsetup #(nbd.overrideAttrs (_: { doCheck = false; }))
    #nyx
    #gnupg
    #aspell aspellDicts.de aspellDicts.en
    #biber
    #gnome.gedit
    #mumble pavucontrol chromium
    #xterm firefox icewm
    #xorg.libxcb xorg.xauth
    #xbill
    #yt-dlp ffmpeg-full
    #tigervnc
    #python3
    #zoom-us
    #mumble
    #imagemagick
    #mpv
    #tdesktop feh inkscape evince mupdf
    #libreoffice gimp
    #gnuplot
  ];
  nixpkgs.config.allowUnfree = true;
  hardware.pulseaudio.enable = true;
  hardware.opengl.enable = true;

  fonts.packages = with pkgs; [
    hack-font ubuntu_font_family
    fira-code cascadia-code
    corefonts
  ];

  environment.variables = {
    EDITOR = "${pkgs.vim}/bin/vim";
  };

  programs.bash.enableCompletion = false;
  programs.bash.interactiveShellInit = ''
    HISTSIZE=10000
    HISTCONTROL=ignorespace:ignoredups
    HISTFILESIZE=200000
  '';

  users.groups = { iblech = { gid = 1000; }; };
  users.users.iblech = {
    isNormalUser = true;
    description = "Ingo Blechschmidt";
    home = "/home/iblech";
    uid = 1000;
    group = "iblech";
    extraGroups = [ "audio" ];
  };

  systemd = {
    services.closeluks = {
      serviceConfig.Type = "oneshot";
      script = ''
        ${pkgs.util-linux}/bin/umount /dev/mapper/home 2>/dev/null || true
        ${pkgs.cryptsetup}/bin/cryptsetup luksClose home || true
      '';
    };

    timers.closeluks = {
      wantedBy = [ "timers.target" ];
      partOf = [ "closeluks.service" ];
      timerConfig.OnCalendar = "*:0/5";
    };

    services.nlab-backup = {
      script = "/nlab-roots/run-and-notify-backup.sh";
      path = with pkgs; [
        bash coreutils curl btrfs-progs gnugrep rsync openssh
      ];
      serviceConfig = {
        User = "root";
      };
      startAt = "*-*-* 03:00:00";
      enable = true;
    };

    sockets.nlab22.listenStreams = [ "22" ];
    sockets.nlab80.listenStreams = [ "80" ];
    sockets.nlab443.listenStreams = [ "443" ];
    services.nlab22 = {
      serviceConfig = {
        ExecStart = "${pkgs.systemd}/lib/systemd/systemd-socket-proxyd 10.0.0.2:22";
      };
      wantedBy = [ "multi-user.target" ];
      requires = [ "nlab22.socket" ];
      after = [ "nlab22.socket" ];
    };
    services.nlab80 = {
      serviceConfig = {
        ExecStart = "${pkgs.systemd}/lib/systemd/systemd-socket-proxyd 10.0.0.2:80";
      };
      wantedBy = [ "multi-user.target" ];
      requires = [ "nlab80.socket" ];
      after = [ "nlab80.socket" ];
    };
    services.nlab443 = {
      serviceConfig = {
        ExecStart = "${pkgs.systemd}/lib/systemd/systemd-socket-proxyd 10.0.0.2:443";
      };
      wantedBy = [ "multi-user.target" ];
      requires = [ "nlab443.socket" ];
      after = [ "nlab443.socket" ];
    };

#    services.sylt = {
#      serviceConfig.Type = "oneshot";
#      script = ''
#        ${pkgs.systemd}/bin/systemd-nspawn -bU -D /sylt2
#      '';
#      wantedBy = [ "multi-user.target" ];
#    };
  };
}
