{config, pkgs, ...}:

{
  imports = [
    <nixpkgs/nixos/modules/installer/cd-dvd/installation-cd-minimal.nix>
    <nixpkgs/nixos/modules/installer/cd-dvd/channel.nix>
  ];

  services.journald.extraConfig = "Storage=volatile";
  boot.kernel.sysctl = { "vm.dirty_writeback_centisecs" = 6000; };
  boot.kernelPackages = pkgs.linuxPackages_4_17;

  networking.hostName = "ada-lovelace";
  networking.firewall.enable = false;
  networking.networkmanager.enable = true;
  networking.networkmanager.wifi.powersave = true;

  i18n = {
    consoleKeyMap = "de";
    defaultLocale = "de_DE.UTF-8";
  };

  time.timeZone = "Europe/Berlin";
  services.timesyncd.servers = [ "ntp1.uni-augsburg.de" "0.nixos.pool.ntp.org" ];

  hardware.pulseaudio.enable = true;

  environment.systemPackages = with pkgs; [
    wget bc vim manpages screen file mosh psmisc unzip pciutils usbutils parallel lsof
    git tig
    nmap iptables ethtool dnsutils wireshark
    wpa_supplicant
    xorg.xgamma xdotool xorg.xkill xcalib redshift xrandr-invert-colors xorg.xwininfo
    xorg.xev xorg.xmodmap xsel xvkbd
    rfkill
    (texlive.combine {
      inherit (texlive) scheme-medium biblatex logreq xstring csquotes tabto-ltx
      soul xypic libertine chngcntr multirow bussproofs breakurl mweights fontaxes;
    })
    biber
    mupdf djview
    feh
    ghc
    gnuplot
    ruby
    python3
    gnumake pandoc
    dillo elinks lynx
    imagemagick
    irssi
    tightvnc
    gifsicle libav
    alsaUtils pavucontrol
    sshfs

    networkmanagerapplet lxpanel
    powertop
    chromium firefoxWrapper
    gimp inkscape
    mpv
    xawtv xorg.xeyes
    xaos
    kobodeluxe
    haskellPackages.ghcid haskellPackages.cabal2nix haskellPackages.cabal-install
    # libreoffice-fresh
    # okular xorg.libxcb libsForQt5.phonon-backend-vlc gifsicle libav

    kate
    python3Packages.numpy python3Packages.pygame
  ];

  fonts.fonts = with pkgs; [
    hack-font ubuntu_font_family
  ];

  environment.variables = {
    EDITOR = "vim";
  };

  users.users.ada = {
    isNormalUser = true;
    description = "Ada Lovelace";
    createHome = true;
    home = "/home/ada";
    uid = 1000;
    initialHashedPassword = "$6$utLZPDNys$nxpqRBobo7NAi9kFs7J8Ar5UN2zJY97.tuavJyk1ACyVoELeUwS3AtU7eCPq.R3Yxtb3GvmpuOuH0xrww0pdp.";
  };

  services.openssh.enable = true;

  services.xserver = {
    enable = true;
    layout = "de";
    xkbVariant = "nodeadkeys";
    # videoDrivers = [ "modesetting" ];
    desktopManager.lxde.enable = true;
    displayManager = {
      slim = {
        enable = true;
        defaultUser = "ada";
        autoLogin = true;
      };
    };
    libinput = {
      enable = true;
      middleEmulation = true;
    };
  };
}
