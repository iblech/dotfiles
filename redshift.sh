#!/usr/bin/env bash

inv="$1"
red="$2"

if [ "$red" = 1 ]; then
  redshift -O 5000
else
  redshift -x
  [ "$inv" = "1" ] && xcalib -invert -alter
fi
