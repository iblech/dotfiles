#!/usr/bin/env bash

set -e

src_dir="$1"
dst_dir="$2"

info() {
  echo "$@" >&2
}

error() {
  info "$@"
  exit 1
}

[ -d "$src_dir" ] || error "source dir doesn't exist: $src_dir"
[ -d "$dst_dir" ] || error "source dir doesn't exist: $dst_dir"

cd "$src_dir"

if [ "$(ls -1 | wc -l)" -eq 0 ]; then
  info "no snapshots to be synced."
  exit
fi

for vol in *; do
  info -n "$vol: "

  if [ -e "$dst_dir/verified-$vol" ]; then
    info "already exists on destination."
  else
    parent="$(comm -12 <(ls -1) <(cd "$dst_dir"; ls -1 | egrep ^verified- | sed -e 's+^verified-++') | sort | tail -n 1)"
    [ -e "$dst_dir/$vol" ] && btrfs subvolume del "$dst_dir/$vol"
    if [ -z "$parent" ]; then
      info -n "syncing without common parent... "
      btrfs send "$vol"
    else
      info -n "syncing with parent $parent... "
      btrfs send -p "$parent" "$vol"
    fi | btrfs receive "$dst_dir/"
    sync "$dst_dir/$vol"
    mv "$dst_dir/$vol" "$dst_dir/verified-$vol"
    info "done."
  fi
done
