export PATH="$PATH:/home/iblech/sshlatex:/home/iblech/instiki-cli"
export INSTIKI_AUTHOR="Ingo Blechschmidt"

alias mupdf="mupdf-x11"
alias a="/home/iblech/.bin/akrasia.sh"
alias l="/home/iblech/.bin/leo.sh"
alias i="/home/iblech/.bin/italian.sh"
alias qr="/home/iblech/.bin/qr.sh"

PS1="\[\033]0;\w\007\]@\[\033[31;1m\]\h\[\033[0m\] \[\033[01;33m\]\W \[\033[35;1m\]λ \[\033[0m\]"
HISTSIZE=10000
HISTCONTROL=ignorespace:ignoredups
HISTFILESIZE=200000

complete -r
