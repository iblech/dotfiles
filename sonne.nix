{ config, pkgs, ... }:

{
  imports =
    [ ./hardware-configuration.nix
    ];

  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.timeout = 1;
  boot.tmp.useTmpfs = true;
  boot.supportedFilesystems = [ "exfat" "ext4" "btrfs" "vfat" "fat" "ntfs" ];

  boot.kernel.sysctl = { "vm.dirty_writeback_centisecs" = 6000; };
  boot.kernelParams = [ "mitigations=off" ];
  services.journald.extraConfig = ''
    Storage=volatile
    RuntimeMaxUse=20M
    SystemMaxUse=50M
  '';

  networking.hostName = "sonne";
  #networking.networkmanager.enable = true;

  sound.enable = true;
  hardware.pulseaudio.enable = true;

  fonts.fonts = [ pkgs.corefonts pkgs.liberation_ttf pkgs.dejavu_fonts ];

  #nixpkgs.overlays = [ (self: super: {
  #  espeak = super.espeak.override { mbrolaSupport = false; pcaudiolibSupport = false; sonicSupport = false; };
  #}) ];

  time.timeZone = "Europe/Berlin";
  services.timesyncd.servers = [ "0.nixos.pool.ntp.org" ];
  services.timesyncd.enable = true;

  nixpkgs.config.allowUnfree = true;
  environment.systemPackages = with pkgs; [
    wget bc vim man-pages screen file mosh unzip parallel binutils-unwrapped
    psmisc pciutils usbutils lsof powertop perl
    git tig
    nmap iptables ethtool dnsutils dsniff arp-scan socat
    xorg.xgamma xdotool xorg.xkill xcalib xrandr-invert-colors xorg.xwininfo
    xorg.xwd xorg.xev xorg.xmodmap xsel xvkbd xorg.xmessage xorg.xeyes
    texlive.combined.scheme-minimal
    ghostscript  # für ps2pdf
    djview mupdf
    wine  # (wine.override { gstreamerSupport = false; })
    feh
    elinks lynx dillo
    imagemagick
    alsaUtils pavucontrol
    sshfs
    chromium firefox  # tor-browser-bundle-bin
    mpv
    gimp inkscape
    xaos  # kobodeluxe neverball stellarium
    libreoffice
    okular xorg.libxcb libsForQt5.phonon-backend-gstreamer
    redshift redshift-plasma-applet
    # dropbox skypeforlinux
    aspellDicts.de
    hunspellDicts.de-de
    hunspellDicts.de-at
    hunspellDicts.de-ch
    # kmail
    #okteta minuet
    spectacle marble kontact kolourpaint
    kdialog khelpcenter  # kdenlive
    dragon vlc
    # kitinerary
    #libsForQt5.pim-data-exporter libsForQt5.pimcommon
    #kdeApplications.mailimporter kdeApplications.mbox-importer
    #kdeApplications.akonadi-import-wizard
    #kdeApplications.spectacle
    kaddressbook
    gwenview krita  # skanlite
    ark
    kcalc
    thunderbird  # (thunderbird.override { debugBuild = true; })
    tigervnc x11vnc xorg.xauth
    poppler_utils
    qt5.qttools
    iotop
    tdesktop
    kfind
    #zoom-us
    adobe-reader
    websocat
    #kate
    borgbackup
  ];

  #documentation.enable = false;
  #documentation.doc.enable = false;

  i18n.defaultLocale = "de_DE.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "de";
  };

  services.openssh.enable = true;
  services.openssh.settings.PermitRootLogin = "yes";
  services.openssh.settings.X11Forwarding = true;

  services.printing.enable = true;
  services.printing.drivers = [ pkgs.hplip ];
  hardware.sane.enable = true;
  hardware.sane.extraBackends = [ pkgs.hplipWithPlugin ];

  programs.bash.enableCompletion = false;

  services.xserver = {
    enable = true;
    layout = "de";
    displayManager.sddm.enable = true;
    displayManager.autoLogin = {
      enable = true;
      user = "gb";
    };
    desktopManager.plasma5.enable = true;
    #displayManager.slim.enable = true;
    #desktopManager.xfce.enable = true;
    libinput = { enable = true; touchpad.middleEmulation = true; };
  };

  users.users.gb = {
    isNormalUser = true;
    extraGroups = [ "wheel" ];
  };

  boot.postBootCommands = ''
    rm -r /home/gb/.cache/* /home/gb/Schreibtisch/*.jpg.part
  '';

  systemd.services.sshtunnel = {
    description = "SSH-Tunnel";
    wantedBy = [ "multi-user.target" ];
    serviceConfig = {
      RestartSec = 10;
      Restart = "on-failure";
      User = "root";
    };
    script = ''
      ${pkgs.openssh}/bin/ssh -NTC -o ServerAliveInterval=30 -o ExitOnForwardFailure=yes -R localhost:1389:localhost:22 tunnel@speicherleck.de
    '';
  };

  systemd.services.wallpaper = rec {
    description = "Hintergrundwechsel";
    startAt = "*:0/2";
    path = [ pkgs.bash pkgs.dbus pkgs.perl ];

    serviceConfig = {
      User = "gb";
      ExecStart = "/home/gb/.bin/set-random-wallpaper.sh";
    };
  };

  systemd.services.local-backup = rec {
    description = "Local backup";
    startAt = "*:0";
    path = [ pkgs.bash pkgs.btrfs-progs ];

    serviceConfig = {
      User = "root";
      ExecStart = "/home/local-backup.sh";
    };
  };

  systemd.services.remote-backup = rec {
    enable = true;
    description = "Remote backup";
    startAt = "daily";
    path = with pkgs; [ bash coreutils curl borgbackup openssh bzip2 ];
    script = "/root/run-and-notify.sh";
    serviceConfig.User = "root";
  };

  services.dovecot2 = {
    enable = true;
    enablePAM = false;
    enablePop3 = false;
    extraConfig = ''
      listen = localhost
      passdb {
        driver = static
        args = nopassword
      }
      userdb {
        driver = static
        args = gid=users uid=gb home=/home/%n
      }
      mail_uid = gb
      mail_gid = users
    '';
    mailLocation = "maildir:/hdd/overspill/Maildir";
  };

  services.samba = {
    enable = true;
    securityType = "user";
    extraConfig = ''
      workgroup = ARBERSTRASSE
      server string = sonne
      netbios name = sonne
      hosts allow = 0.0.0.0/0
      guest account = nobody
      map to guest = bad user
    '';
    shares = {
      public = {
        path = "/home/dateiaustausch";
        browseable = "yes";
        "read only" = "no";
        "guest ok" = "yes";
        "create mask" = "0644";
        "directory mask" = "2755";
        "force directory mode" = "2775";
        "force user" = "nobody";
        "force group" = "nogroup";
        "public" = "yes";
        "writable" = "yes";
      };
    };
  };

  nixpkgs.config.permittedInsecurePackages = [
    "adobe-reader-9.5.5"
  ];

  system.stateVersion = "22.05";

  hardware.opengl.enable = false;
}
