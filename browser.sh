#!/usr/bin/env bash

[ -e ~/.config/chromium ] || rsync --delete -av /persistent/home/user/.config/chromium-base/ /home/user/.config/chromium

chromium --disk-cache-dir=/tmp

rsync --delete -av /home/user/.config/chromium/ /persistent/home/user/.config/chromium-base/
