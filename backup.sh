#!/usr/bin/env bash

set -e

nc -z localhost 10809 &>/dev/null || ssh -f -N -L10809:localhost:10809 tunnel@173.249.60.202

echo -n "Waiting for port forwarding to show up... "
until nc -z localhost 10809 &>/dev/null; do
  echo -n .
  sleep 1
done
echo "good."

echo -n "Connecting nbd... "
modprobe nbd
nbd-client -c /dev/nbd1 >/dev/null || nbd-client -g -name ingo localhost /dev/nbd1
echo "good."

echo -n "Waiting for nbd to become available... "
until [ "$(head -c 4 /dev/nbd1)" = "LUKS" ]; do
  echo -n .
  sleep 1
done
echo "good."

echo -n "Opening luks... "
[ -e /dev/mapper/cloud ] || cryptsetup luksOpen --key-file=/home/keyfile-cloud /dev/nbd1 cloud
echo "good."

echo -n "Mounting volume... "
mkdir -p /cloud
mount | grep clou[d] >/dev/null || mount -t btrfs /dev/mapper/cloud /cloud
echo "good."

echo -n "Creating a snapshot... "
btrfs subvolume snapshot -r /home/iblech /home/snapshots/$(date -u -Iseconds)
echo "good."

echo "Syncing all snapshots..."
/home/sync-snapshots.sh /home/snapshots /cloud/snapshots
echo "good."

echo -n "Unmounting volume... "
umount /cloud
echo "good."

echo -n "Closing luks... "
cryptsetup luksClose cloud
echo "good."

echo -n "Disconnecting nbd... "
nbd-client -d /dev/nbd1
echo "good."
