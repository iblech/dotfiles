{ config, pkgs, fetchurl, lib, ... }:

let
  mytexlive = pkgs.texlive.combine {
    inherit (pkgs.texlive) scheme-medium biblatex logreq xstring csquotes tabto-ltx
    soul xypic libertine chngcntr multirow bussproofs breakurl mweights
    fontaxes environ framed trimspaces adjustbox collectbox tikz-cd comment
    moderncv fontawesome ifsym a4wide cleveref arev shadethm blindtext
    todonotes mdframed bbding titlesec wrapfig makecell enumitem nopageno
    needspace stix subfigure footmisc lkproof ocgx2 media9 ucs dirtytalk
    tcolorbox tikzpagenodes ifoddpage fdsymbol kurier preview cjk dashrule
    ifmtarg eepic multibib graphbox datatool xfor substr pgfplots
    draftwatermark everypage minted newunicodechar cancel svg ifplatform
    mnsymbol cmll endfloat xpatch fvextra upquote calligra accents
    boxedminipage animate zref marginnote bbm ntheorem type1cm newtx;
  };

  mytigervnc = pkgs.tigervnc;

  myagda =
    pkgs.agda.withPackages (p: [ p.standard-library p.cubical p.agda-categories ]);

  myemacs = pkgs.emacsWithPackages (epkgs: [ epkgs.evil epkgs.tramp-theme epkgs.ahungry-theme ]);
  myemacs-nox = pkgs.emacs-nox.pkgs.withPackages (epkgs: [ epkgs.evil epkgs.tramp-theme epkgs.ahungry-theme ]);
in {
  system.stateVersion = "18.09";
  nix.settings.sandbox = true;

  disabledModules = [ "services/networking/libreswan.nix" ];

  nixpkgs.overlays = [
    (self: super: {
      libreswan = super.libreswan.overrideAttrs (old: rec {
        NIX_CFLAGS_COMPILE = toString [ "-Wno-error=redundant-decls" "-Wno-error=format-nonliteral"
            # these flags were added to build with gcc7
            "-Wno-error=implicit-fallthrough"
            "-Wno-error=format-truncation"
            "-Wno-error=pointer-compare"
            "-Wno-error=stringop-truncation"
            "-DNSS_PKCS11_2_0_COMPAT=1"
        ];
      });
    })
  ];

  imports =
    [ ./hardware-configuration.nix
      # /tmp-iblech/nixpkgs/nixos/modules/services/web-apps/whitebophir.nix
      # /tmp-iblech/libreswan.nix
    ];

  services.whitebophir = {
    enable = true;
    # package = pkgs.callPackage /tmp-iblech/nixpkgs/pkgs/servers/web-apps/whitebophir {};
  };

  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/sda";
  boot.loader.grub.configurationLimit = 5;
  boot.loader.timeout = 10;
  boot.tmp.useTmpfs = true;

  boot.kernel.sysctl = {
    "vm.dirty_writeback_centisecs" = 6000;
    "fs.inotify.max_user_watches" = 81920;
    "fs.file-max" = 65536;
  };
  boot.kernelPackages = pkgs.linuxPackages_latest;
  services.journald.extraConfig = "SystemMaxUse=200M";

  networking.hostName = "quasicoherent";
  networking.dhcpcd.enable = false;
  networking.firewall.enable = false;
  networking.interfaces.ens18.ipv4.addresses = [ { address = "79.143.177.192"; prefixLength = 24; } ];
  networking.defaultGateway = "79.143.177.1";
  networking.interfaces.ens18.ipv6.addresses = [ { address = "2a02:c205:3003:5755::1"; prefixLength = 64; } ];
  networking.defaultGateway6 = { address = "fe80::1"; interface = "ens18"; };
  networking.nameservers = [ "79.143.183.251" "79.143.183.252" ];

  services.openssh.ports = [ 223 ];

  networking.extraHosts = ''
    173.249.60.202 speicherleck.de
  '';

  hardware.pulseaudio.enable = true;

  time.timeZone = "Europe/Berlin";
  services.timesyncd.servers = [ "0.nixos.pool.ntp.org" ];

  services.ejabberd.enable = false;
  # services.ejabberd.package = pkgs.callPackage /tmp-iblech/ejabberd {};
  services.ejabberd.configFile = "/home-ejabberd/ejabberd.yml";

  environment.systemPackages = with pkgs; [
    wget bc vim man-pages screen file mosh unzip parallel binutils-unwrapped
    perl
    psmisc pciutils usbutils lsof powertop
    git tig
    nmap iptables ethtool dnsutils arp-scan socat
    mutt procmail fetchmail
    sshfs
    lynx elinks
    cryptsetup # (nbd.overrideAttrs (_: { doCheck = false; }))
    nyx
    gnupg
    #aspell aspellDicts.de aspellDicts.en
    #mumble pavucontrol
    #emacs # myagda
    #mytigervnc xterm icewm
    # firefox chromium
    #xorg.libxcb
    #xbill
    #youtube-dl ffmpeg
    borgbackup
    rclone
  ];

  environment.variables = {
    EDITOR = "vim";
  };

  programs.bash.enableCompletion = false;
  programs.bash.interactiveShellInit = ''
    HISTSIZE=10000
    HISTCONTROL=ignorespace:ignoredups
    HISTFILESIZE=200000
  '';

  users.groups = { iblech = { gid = 1000; }; };
  users.users.iblech = {
    isNormalUser = true;
    description = "Ingo Blechschmidt";
    home = "/home/iblech";
    uid = 1000;
    group = "iblech";
    extraGroups = [ "docker" "audio" ];
  };
#  users.users.catarina = { isNormalUser = true; description = "Agda is fun"; home = "/home/fun/catarina"; uid = 2000; };
#  users.users.julia    = { isNormalUser = true; description = "Julia";       home = "/home/fun/julia";    uid = 3000; };
#  users.users.fun      = { isNormalUser = true; description = "Fun";         home = "/home/fun/fun";      uid = 4000; };
#  users.users.jannis   = { isNormalUser = true; description = "Jannis";      home = "/home/fun/jannis";   uid = 5000; };
  users.users.guest    = { isNormalUser = true; description = "Guest";       home = "/home-boxes";        uid = 10000; };

  virtualisation.docker.enable = false;

  services.openssh.enable = true;
  services.openssh.settings.PermitRootLogin = "yes";
  services.openssh.settings.X11Forwarding = true;
  services.openssh.extraConfig = ''
    AcceptEnv LANG LC_*
    TCPKeepAlive yes
  '';

  services.borgbackup.repos = {
    gb = {
      authorizedKeysAppendOnly = [
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDLiKcy/35Yw6jzJhVUNjrcfeuDvvIM+7LZC8dSke0mrWPU1NZ15casiZWZGEvVIg9RzWJMEFzF5FTt1SjYhajA09cxlReAWwEz5+XzRhIr0QwrzXgbXZOkBeQLS8qBb2HtMPU+wJd+3p0Jum+gkJAgDwbQdQVn0o+peuclX5wIsCNHIoyxfOwJaqA9h7+FjugefLcEl1Z4MgrhP8d8nHtotIU/HCB18hpI3q3ylN/jBZpb8Q4L0wxfqX8PS94vShoD6qpA8BCqmqMEBHtmtBzicdbatWbDHFHXJD5vcftOqoZ4yPTbq3wAv4QQt5/WBm48pH8YZZfSGqUAAUtNfrnYh+0YRmIlCok99P8XGOGBHZI4SjEt2ZqyaKNy8Yorri7Rae5+E0xLWfwVOVA+eX2nVAf1lw4D8itX6HEDCVhhuHN69d0RjEGpQ1YL/fIy2z1xO1UQc2HucLVOG+RFMQoK4AHPRJVr0+etrzxWs0+vYKDvRRtsC2CmIOJWXT1pKNc= root@box"
      ];
      path = "/silo-gb";
    };
    iblech-speicherleck = {
      authorizedKeysAppendOnly = [
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDqIVmkFLTzPm4b2cZyE8Ofd1vLxsA75idIxaOc0HB+e3jmpHCUpks8MD/VEo8TDbaRPZ33FsRsUczG0g++kdMr4h0u8Pvt9M6D0aDyffe1mSIcQNsx50VNvPauVjYqezQhU4roigDccGne8jN28GiHi4lSxJnXbT0W+7u6RS1aD031bhly4GFYA6sJ41OslDVNVJ4ZIXDVz/BJxQdrenRysYQPPB7i0npZ6rI9P8eueDUltkMvtzh8RF1y4r3Q5abzAOFSKcSJSCgJNqEsIiVPgWjIazJV5YKQZXCR11GUZ9pBkOIJ2ZGGMvepF95InE3HYBBYpeX9v18SL4iwRNa/ root@speicherleck.de"
      ];
      path = "/silo-iblech/speicherleck";
    };
    iblech-laptop = {
      authorizedKeysAppendOnly = [
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCuo33kfHx42dfPPIzwV9b80LFSANPxsehOzXsVQTv9hcNuv8AagK9XKy12V8i1HYHZtqTLfE4EO1bVss18IvCoSA/0kuvB2Cq/aPMbq4Lu+c9UvL1YNIFb5nvH6g+/hpQOHqJfaH687cuWq4cOfIjSHVwuHwScgqJYE0sIsyb51GbyBF7OeY5I0ZNCPK7CZNO5MfQkvjERCmmgeYLXG7TDY+fNUToAfP9ZpVjgNx98t3fUoi0ydaoIaWGh0o+l2wUV8AhOCA4mYJFrtBudawt7z8u4yfNt54+HO5Z+ZQ90zqzH83lqkopthRiQyVcltnWbmXkBe17kyYujyNIMdjD81hEH/px/m8PuyK+K9ElOZGwFNEa5hsDvYExNJR93w3NjCkUGJLW4KwEkxc/eqem00p8oEliSCXGqtLGLSg4c9Dv49E0RApqwTaUIwd53v+/PVv0yIpdXrY/9279dDUXlOxmdVQRdM8CRkDzuO/bLCFH443eFN6ClhBnY8oiwuUU= user@lila"
      ];
      path = "/silo-iblech/laptop";
    };
  };

  systemd = {
#    services.openluks = {
#      description = "open luks for /home";
#      script = ''
#        set -e
#        [ -e /dev/mapper/crypted ] || ${pkgs.cryptsetup}/bin/cryptsetup luksOpen /dev/sda4 crypted --key-file <(cat </dev/tcp/localhost/1024)
#      '';
#      wantedBy = [ "home.mount" ];
#    };

#    services.closeluks = {
#      serviceConfig.Type = "oneshot";
#      script = ''
#        if ${pkgs.util-linux}/bin/umount /dev/mapper/crypted 2>/dev/null; then
#          ${pkgs.cryptsetup}/bin/cryptsetup luksClose crypted
#        fi
#      '';
#    };
#
#    timers.closeluks = {
#      wantedBy = [ "timers.target" ];
#      partOf = [ "closeluks.service" ];
#      timerConfig.OnCalendar = "*:0/2";
#    };
  };

  services.murmur.enable = false;

  services.tor = {
    enable = true;
    controlSocket.enable = true;
    relay.enable = true;
    relay.role = "relay";
    settings = {
      Address = "79.143.177.192";
      BandwidthRate = 4*1024*1024;
      BandwidthBurst = 7*1024*1024;
      ContactInfo = "iblech@quasicoherent.io";
      Nickname = "iblech";
      ORPort = 443;
      SOCKSPort = [ 9063 ];
    };
    #extraConfig = ''
    #  ORPort [2a02:c205:3003:5755::1]:443
    #'';
    client.enable = true;
    relay.onionServices.myOnion = {
      map = [
        { port = 80; target = { addr = lib.removeSuffix "\n" (builtins.readFile "/root/myonion"); port = 80; }; }
      ];
    };
  };

  systemd.additionalUpstreamSystemUnits = lib.optional config.boot.tmp.useTmpfs "tmp.mount";

  services.iodine.server = {
    enable = true;
    domain = "zone.quasicoherent.io";
    ip = "172.16.0.1/24";
    passwordFile = "/root/iodine.pass";
    extraConfig = "-c";
  };

  services.privoxy.enable = false;
  services.privoxy.settings = {
    forward-socks4a = "/ 127.0.0.1:9063 .";
    toggle = false;
    enable-remote-toggle = false;
    enable-edit-actions = false;
    enable-remote-http-toggle = false;
  };

  systemd.services.sshtunnel = {
    serviceConfig = {
      ExecStart = "${pkgs.openssh}/bin/ssh -o ServerAliveInterval=15 -T -Rlocalhost:8888:box.containers:80 -Rlocalhost:5001:localhost:5001 tunnel@speicherleck.de";
      User = "fff";
      Restart = "always";
      RestartSec = "10";
    };
    wantedBy = [ "multi-user.target" ];
  };

#  systemd.services.linuxpad-tor1 = {
#    requires = [ "privoxy.service" ];
#    serviceConfig = {
#      ExecStart = "${pkgs.socat}/bin/socat UNIX-LISTEN:${lib.removeSuffix "\n" (builtins.readFile "/root/linuxpad-tor-socket-path1")},fork TCP:localhost:8118";
#    };
#    wantedBy = [ "multi-user.target" ];
#  };
#  systemd.services.linuxpad-tor2 = {
#    requires = [ "privoxy.service" ];
#    serviceConfig = {
#      ExecStart = "${pkgs.socat}/bin/socat UNIX-LISTEN:${lib.removeSuffix "\n" (builtins.readFile "/root/linuxpad-tor-socket-path2")},fork TCP:localhost:8118";
#    };
#    wantedBy = [ "multi-user.target" ];
#  };

#  containers.torgw = {
#    config =
#      {config, pkgs, ...}:
#      {
#        services.journald.extraConfig = ''
#          Storage=volatile
#          RuntimeMaxUse=20M
#        '';
#
#        time.timeZone = "Europe/Berlin";
#        time.hardwareClockInLocalTime = true;
#        networking.firewall.enable = false;
#
#        networking.nameservers = [ "10.0.0.1" ];
#
#        systemd.services.tun2socks = {
#          wantedBy = [ "multi-user.target" ];
#          description = "tun2socks";
#          serviceConfig = {
#            ExecStart = "${pkgs.badvpn}/bin/badvpn-tun2socks --tundev tun0 --netif-ipaddr 169.254.1.2 --netif-netmask 255.255.255.252 --socks-server-addr 10.0.0.1:9063";
#            ExecStartPre = "${pkgs.bash}/bin/bash -c '${pkgs.iproute}/bin/ip tuntap add dev tun0 mode tun; ${pkgs.iproute}/bin/ip link set tun0 up; ${pkgs.iproute}/bin/ip addr add 169.254.1.1/30 dev tun0'";
#            ExecStartPost = "${pkgs.bash}/bin/bash -c '${pkgs.nettools}/bin/route del -net default; ${pkgs.nettools}/bin/route add default gw 169.254.1.2'";
#          };
#        };
#
#        services.strongswan = {
#          enable = true;
#          connections.l2tp-psk = {
#            authby = "secret";
#            pfs = "no";
#            auto = "add";
#            rekey = "no";
#            type = "transport";
#            left = "10.0.0.2";
#            leftprotoport = "17/1701";
#            right = "%any";
#            rightprotoport = "17/%any";
#            rightsubnet = "vhost:%priv,%no";
#          };
#          secrets = [ "/etc/ipsec.camp.secrets" ];
#        };
#
#        services.xl2tpd = {
#          enable = true;
#          clientIpRange = "172.16.0.100-200";
#          extraXl2tpOptions = ''
#            assign ip = yes
#            require chap = yes
#            refuse pap = yes
#            require authentication = yes
#            name = OpenswanVPN
#            ppp debug = yes
#          '';
#          extraPppdOptions = ''
#            ms-dns 10.0.0.1
#          '';
#        };
#      };
#    ephemeral = true;
#    autoStart = true;
#    enableTun = true;
#    privateNetwork = true;
#    hostAddress = "10.0.0.1";
#    localAddress = "10.0.0.2";
#    bindMounts = { "/etc/ipsec.camp.secrets" = { hostPath = "/etc/ipsec.camp.secrets"; isReadOnly = true; }; };
#    additionalCapabilities = [ "CAP_MKNOD" ];
#  };
#
#  systemd.services.torgw-misc =
#  let torgwinit = pkgs.writeScript "torgwinit" ''
#    #!${pkgs.bash}/bin/bash
#
#    set -e
#
#    PATH=$PATH:${pkgs.socat}/bin:${pkgs.systemd}/bin
#
#    socat TCP-LISTEN:9063,bind=10.0.0.1,reuseaddr,fork TCP:127.0.0.1:9063 &
#    socat UDP-LISTEN:53,bind=10.0.0.1,reuseaddr,fork UDP:127.0.0.1:9053 &
#
#    for i in 68 500 4500; do
#      socat UDP-LISTEN:$i,reuseaddr,fork UDP:10.0.0.2:$i &
#    done
#
#    sleep 30
#
#    echo 'c 108:* rwm' > /sys/fs/cgroup/devices/machine.slice/container\@torgw.service/devices.allow 
#    machinectl shell torgw ${pkgs.bash}/bin/bash -c '
#      set -e
#      [ -e /dev/ppp ] || mknod /dev/ppp c 108 0
#      mkdir -p /etc/xl2tpd/ppp
#      echo "* * kohle123 *" > /etc/xl2tpd/ppp/chap-secrets
#      echo 1 > /proc/sys/net/ipv4/ip_forward
#      ${pkgs.iptables}/bin/iptables -F
#      ${pkgs.iptables}/bin/iptables -F -t nat
#      ${pkgs.iptables}/bin/iptables -A FORWARD -j ACCEPT
#      ${pkgs.iptables}/bin/iptables -t nat -A POSTROUTING -j MASQUERADE
#    '
#
#    wait
#  ''; in
#  {
#    requires = [ "container@torgw.service" ];
#    serviceConfig = { ExecStart = "${torgwinit}"; };
#    wantedBy = [ "multi-user.target" ];
#  };

#  containers.box = {
#    config =
#      {config, pkgs, ...}:
#      {
#        imports = [ /tmp-iblech/agdapad/backend/container.nix ];
#
#        containers.xskeleton.config = {
#          hardware.pulseaudio.enable = true;
#          environment.systemPackages = with pkgs; [
#            bash bastet bc chromium djview evince feh firefoxWrapper gimp
#            git gnome3.gedit gnuplot golly gtypist imagemagick inkscape
#            libreoffice moon-buggy mupdf zathura nano manpages
#            redshift screen socat sshfs texmaker texstudio
#            tig mytigervnc unzip vim wget xaos xcalib xdotool xorg.xauth xorg.xev
#            xorg.xgamma xorg.xkill xorg.xmessage xorg.xmodmap xorg.xwd
#            xorg.xwininfo xorg.libxcb xrandr-invert-colors xsel xvkbd
#            (pkgs.python3.withPackages (self: [ self.pygame self.numpy self.matplotlib self.pygments self.pip ]))
#            #mytexlive
#            icewm
#            #atom gnome3.gedit geany
#            #wineFull
#            mumble pavucontrol
#            #mono gcc
#            #enigma blobwars stellarium fish-fillets-ng freedroid freedroidrpg frozen-bubble krita
#            #gnujump pingus tuxpaint xbill jumpnbump
#            #racket
#            htop
#          ];
#          services.xserver.desktopManager.xfce.enable = true;
#          fonts.fontconfig.enable = true;
#          fonts.fonts = with pkgs; [ hack-font ubuntu_font_family ];
#        };
#      };
#    ephemeral = true;
#    autoStart = true;
#    privateNetwork = true;
#    hostAddress = "192.168.0.1";
#    localAddress = "192.168.0.2";
#    bindMounts = { "/home" = { hostPath = "/home-boxes"; isReadOnly = false; }; };
#  };

  services.davfs2.enable = true;
}
