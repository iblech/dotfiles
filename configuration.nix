{ config, pkgs, lib, fetchurl, ... }:

let
  impermanence = builtins.fetchTarball {
    url = "https://github.com/nix-community/impermanence/archive/4b3e914cdf97a5b536a889e939fb2fd2b043a170.tar.gz";
    sha256 = "sha256:04l16szln2x0ajq2x799krb53ykvc6vm44x86ppy1jg9fr82161c";
  };

  vibrant-cli = pkgs.fetchFromGitHub {
    owner = "libvibrant";
    repo = "libvibrant";
    rev = "1.1.1";
    hash = "sha256-APja211+U0WVuCRz8f3VIAQLF4oPhh0CJ3Y5EgSJnh0=";
  };

  my-mupdf = pkgs.mupdf.overrideAttrs (attrs: { patches = attrs.patches ++ [
    (pkgs.fetchurl {
      url = "https://www.speicherleck.de/iblech/stuff/mupdf-bgcolor-1_24_9.patch";
      hash = "sha256-J3+/YzMDEmSMVXG6pn0BH3Nsiq8bDTFtdnTVnN/4j0g=";
    })
  ]; });

  box = {
    config =
      { config, pkgs, ... }:
      {
        system.stateVersion = "${config.system.nixos.release}";
        environment.systemPackages = with pkgs; [
          arp-scan dnsutils ethtool iproute2 iptables perl psmisc socat tcpdump vim
        ];
        networking.firewall.enable = false;
        documentation.doc.enable = false;
        services.speechd.enable = false;
        networking.networkmanager.enable = true;
      };
    ephemeral = true;
    privateNetwork = true;
    extraFlags = [ "--network-veth" ];
  };
in
{
  imports =
    [ ./hardware-configuration.nix
      "${impermanence}/nixos.nix"
    ];

  nix.extraOptions = ''
    experimental-features = nix-command flakes
  '';

  boot.supportedFilesystems = [ "btrfs" "ext4" "vfat" "ntfs" ];
  boot.initrd.systemd.services.persisted-files = {
    description = "Hard-link persisted files from /persistent";
    wantedBy = [ "initrd.target" ];
    after = [ "sysroot.mount" ];
    unitConfig.DefaultDependencies = "no";
    serviceConfig.Type = "oneshot";
    script = ''
      mkdir -p /sysroot/etc
      ln -snfT /persistent/etc/machine-id /sysroot/etc/machine-id
    '';
  };

  environment.persistence."/persistent" = {
    directories = [
      "/etc/nixos"
      "/var/log"
      "/var/lib/alsa"
      "/var/lib/cups"
      "/var/lib/iwd"
      "/var/lib/nixos"
      "/etc/NetworkManager"
      "/root/.wg"
    ];
    files = [
      # "/etc/machine-id"
      "/root/.vimrc"
      "/root/.bash_history"
    ];
    users.user = {
      directories = [
        ".local/share/TelegramDesktop"
        ".local/share/fonts"
        { directory = ".ssh"; mode = "0700"; }
        ".bin"
        ".xmonad"
	".agda"
        ".config/discord"
	".config/wg"
	".config/Element"
	".config/Signal"
	".config/totp-cli"
	".cache/borg"
        "Downloads/Telegram Desktop"
        "persistent"
      ];
      files = [
        ".screenrc"
        ".vimrc"
        ".bashrc"
        ".Xdefaults"
        ".gitconfig"
        ".bash_history"
	".emacs"
      ];
    };
  };

  nixpkgs.config.allowUnfree = true;

  boot.loader.grub.enable = true;
  boot.loader.timeout = 1;
  boot.consoleLogLevel = 9;
  boot.kernelParams = [ "mitigations=off" "boot.shell_on_fail" "systemd.default_timeout_start_sec=30s" "rootdelay=30" ];

  services.journald.extraConfig = ''
    Storage=volatile
    RuntimeMaxUse=20M
  '';
  boot.kernel.sysctl = {
    "vm.dirty_writeback_centisecs" = 6000;
    "vm.dirty_expire_centisecs" = 6000;
  };
  boot.kernelPackages = pkgs.linuxPackages_latest;
  boot.extraModulePackages = [ config.boot.kernelPackages.v4l2loopback ];
  boot.postBootCommands = ''
    ${pkgs.iw}/bin/iw dev wlan0 set power_save on
  '';

  systemd.services."rfkill-on-boot" = {
    restartIfChanged = false;
    description = "Rfkill on boot";
    wantedBy = [ "multi-user.target" ];
    serviceConfig = {
      ExecStart = "${pkgs.util-linux}/bin/rfkill block nfc bluetooth";
      Type = "oneshot";
      RemainAfterExit = "true";
    };
  };

  #ACTION=="add|change", SUBSYSTEM=="usb", ATTRS{idVendor}=="062a", ATTRS{idProduct}=="4101", ATTR{power/control}:="on"
  #ACTION=="add|change", SUBSYSTEM=="usb", ATTRS{idVendor}=="04d9", ATTRS{idProduct}=="0024", TEST=="power/control", ATTR{power/control}:="on"
  boot.initrd.services.udev.rules = ''
    ACTION=="add|change", SUBSYSTEM=="usb", ATTR{product}=="USB Keyboard", TEST=="power/control", ATTR{power/control}="on"
  '';
  services.udev.extraRules = ''
    ACTION=="add|change", SUBSYSTEM=="usb", ATTR{product}=="USB Keyboard", TEST=="power/control", ATTR{power/control}="on"
  '';

  powerManagement.cpuFreqGovernor = "performance";
  powerManagement.powertop.enable = true;
  services.tlp.enable = true;
  #services.logind.lidSwitch = "ignore";

  systemd.extraConfig = ''
    DefaultTimeoutStartSec=30s
    DefaultTimeoutStopSec=30s
  '';

  networking.hostName = "arolla";
  networking.firewall.enable = false;
  #networking.nameservers = [ "8.8.8.8" ];
  boot.blacklistedKernelModules = [ "r8169" "acer-wmi" "btusb" "e1000e" "nfc" ];  # Ethernet needs 4 W of power
  boot.extraModprobeConfig = ''
    options snd_hda_intel power_save=1
  '';
  networking.dhcpcd.extraConfig = ''
    noarp
    nodelay
    background
  '';
  networking.networkmanager.enable = lib.mkForce false;
  networking.wireless.iwd.enable = true;
  networking.wireless.iwd.settings = {
    General.AddressRandomization = "once";
  };
  networking.nameservers = [ "127.0.0.1" ];
  services.usbmuxd.enable = true;

  networking.extraHosts = ''
    173.249.60.202 speicherleck.de
    79.143.177.192 quasicoherent.io
    62.171.174.139 agdapad.quasicoherent.io
    2a02:c207:3003:5753::1 six.speicherleck.de
    2a02:c205:3003:5755::1 six.quasicoherent.io
  '';

  console.keyMap = "de";
  i18n = {
    defaultLocale = "en_US.UTF-8";
    extraLocaleSettings.LC_TIME = "C.UTF-8";
    extraLocaleSettings.LC_PAPER = "de_DE.UTF-8";
  };

  time.timeZone = "Europe/Berlin";
  services.timesyncd.servers = [ "ntp1.uni-augsburg.de" "0.nixos.pool.ntp.org" ];

  environment.systemPackages = with pkgs; [
    btrfs-progs
    wget bc vim man-pages screen tmux file mosh unzip binutils-unwrapped
    psmisc pciutils usbutils lsof powertop lm_sensors config.boot.kernelPackages.cpupower
    git tig
    btdu duperemove
    nmap iptables ethtool dnsutils arp-scan dsniff socat iodine iw jq openssl wirelesstools wireshark tcpdump mtr hostapd #nbd
    lxterminal
    wpa_supplicant macchanger
    qrencode
    xorg.xgamma xdotool xorg.xkill xcalib redshift xrandr-invert-colors xorg.xwininfo
    xorg.xwd xorg.xev xorg.xmodmap xsel xvkbd xorg.xmessage
    (texlive.combine {
      inherit (texlive)
      scheme-small pdftex pdfjam
      a4wide accents adjustbox amsrefs animate arev arydshln avantgar bbding bbm bbm-macros
      biblatex bibtex bibexport blindtext boxedminipage breakurl bussproofs calligra
      cancel catchfile chngcntr cjk cleveref cmll cm-super collectbox comment csquotes
      dashrule datatool dirtytalk draftwatermark eepic endfloat enumitem
      environ everypage extpfeil fdsymbol fontawesome fontawesome5 fontaxes
      footmisc framed fvextra graphbox here hypdoc hyphenat ifmtarg ifoddpage ifplatform ifsym
      kurier lastpage lazylist libertine lkproof logreq makecell manfnt
      marginnote marvosym mdframed media9 minted mnsymbol moderncv multibib multirow
      mweights needspace newtx newunicodechar nopageno ntheorem ocgx2 pgfplots
      polytable preview shadethm siunitx soul stix stmaryrd sttools subfigure
      substr svg tabto-ltx tcolorbox thmtools tikz-cd tikzfill tikzmark
      tikzpagenodes titlesec todonotes trimspaces type1cm ucs upquote vwcol
      wasy wasysym wrapfig xfor xifthen xpatch xstring xypic zref txfonts;
    })
    biber
    djview my-mupdf # zathura
    feh
    (haskellPackages.ghcWithPackages (pkgs: with pkgs; [random]))
    gnuplot
    #ruby
    gnumake pandoc
    lynx elinks
    imagemagick ghostscript
    #irssi
    alsa-utils pavucontrol
    sshfs
    screen-message
    sshlatex

#   (pkgs.python2.withPackages (self: [ self.pygame ]))
    (pkgs.python3.withPackages (self: [ self.pygame self.flask self.mariadb self.mysql-connector self.packaging self.numpy self.matplotlib self.pygments self.scipy ]))
    python3

    openconnect

    chromium
    powertop
    #firefox
    gimp inkscape gedit evince
    mpv
    xorg.xeyes xteddy
    xaos
    # kobodeluxe # neverball # stellarium  # pingus
    haskellPackages.ghcid haskellPackages.cabal-install  # haskellPackages.cabal2nix
    libreoffice
    okular xorg.libxcb libsForQt5.phonon-backend-gstreamer gifsicle
    inotify-tools poppler_utils
    aspell aspellDicts.de aspellDicts.en
    yt-dlp
    ffmpeg #-full
#   fgallery

    (agda.withPackages (p: [ p.standard-library p.cubical p.agda-categories ]))
    (emacs.pkgs.withPackages (epkgs: [ epkgs.polymode epkgs.markdown-mode epkgs.evil epkgs.tramp-theme epkgs.ahungry-theme epkgs.color-theme-sanityinc-tomorrow ]))
    # coq
    # lean

    signal-desktop
    tdesktop
    element-desktop
    signal-desktop
    (discord.override { withTTS = false; })

    # tangogps

    # mumble
    websocat
    tigervnc

    #zoom-us #teams

#   obs-studio
#   lxterminal
#   asciinema

    tigervnc

    #xvkbd

    #hugo

#   freetube

    ripgrep
    shellcheck
    borgbackup
    flameshot
    gnupg
    zoxide

    #diffoscopeMinimal
    bsdgames

    rxvt-unicode

    (pkgs.callPackage "${vibrant-cli}/nix/package.nix" {})

    xawtv
    totp-cli
  ];

  fonts.packages = with pkgs; [
    hack-font ubuntu_font_family
    fira-code victor-mono
    corefonts
    inter open-dyslexic open-sans roboto-mono noto-fonts-lgc-plus
    inriafonts
    jetbrains-mono
  ];

  environment.variables = {
    EDITOR = "vim";
  };

  programs.bash.completion.enable = false;
  programs.bash.interactiveShellInit = ''
    HISTSIZE=10000
    HISTCONTROL=ignorespace:ignoredups
    HISTFILESIZE=200000
  '';

  users.groups = { user = { gid = 1000; }; };
  users.users.user = {
    isNormalUser = true;
    description = "";
    createHome = true;
    home = "/home/user";
    uid = 1000;
    group = "user";
    extraGroups = [ "video" "audio" "adbusers" "plugdev" "davfs2" "dav_group" "netdev" "wheel" "networkmanager" ];
    hashedPassword = "$y$j9T$CKbE8Jwk2cGpd9f90yUyU1$zAANXQ8bGMEmpO4xfgM3UJ9cZGtT3IxKkI2BqK0BRC6";
  };
  users.mutableUsers = false;
  users.users.root.hashedPassword = "$y$j9T$CKbE8Jwk2cGpd9f90yUyU1$zAANXQ8bGMEmpO4xfgM3UJ9cZGtT3IxKkI2BqK0BRC6";

  services.davfs2.enable = true;

  services.openssh.enable = false;
  services.openssh.settings.PermitRootLogin = "no";

  services.xserver = {
    enable = true;
    xkb.layout = "de";
    xkb.variant = "nodeadkeys";
    windowManager.xmonad = {
      enable = true;
      enableContribAndExtras = true;
    };
    digimend.enable = false;
  };
  services.displayManager = {
    defaultSession = "none+xmonad";
    autoLogin = {
      enable = true;
      user = "user";
    };
  };
  services.libinput = {
    enable = true;
    touchpad.middleEmulation = true;
  };

  services.printing.enable = true;
  services.printing.startWhenNeeded = true;
  services.printing.drivers = with pkgs; [ gutenprint gutenprintBin hplip epson-escpr2 ];

#  services.tor.enable = true;
#  services.tor.settings = ''
#    Socks4Proxy localhost:1234
#  '';
#  services.tor.client.enable = true;

  services.privoxy.enable = false;
  services.privoxy.settings = {
    forward-socks5t = "/ 127.0.0.1:9063 .";
    # ;-(
    forward = [
      #"google.* ."
      #"*.google.* ."
      #"*.google.*.* ."
      #"*.googleusercontent.com ."
      "wifionice.de ."
      "iceportal.de ."
      "*.wifionice.de ."
      "*.iceportal.de ."
      "latein.at ."
      "*.latein.at ."
      "hotspot.vodafone.de ."
      "*.readthedocs.io ."
      "gitlab.com ."
      "127.0.0.1 ."
      "10.*.*.* ."
      "192.168.*.* ."
      "localhost ."
    ];
    toggle = false;
    enable-remote-toggle = false;
    enable-edit-actions = false;
    enable-remote-http-toggle = false;
  };

  system.stateVersion = "24.05";

  xdg.portal = {
    enable = false;
    config.common.default = "*";
    extraPortals = []; # pkgs.xdg-desktop-portal-gtk ];
  };

  services.stubby = {
    enable = true;
    settings = pkgs.stubby.passthru.settingsExample // {
      upstream_recursive_servers = [{
	address_data = "5.9.164.112";
	tls_auth_name = "dns3.digitalcourage.de";
	tls_pubkey_pinset = [{
	  digest = "sha256";
	  value = "2WFzfO2/56HpeR+v/l25NPf5dacfxLrudH5yZbWCfdo=";
	}];
      }];
    };
  };

  # systemctl start wg-quick-wg0.service
  networking.wg-quick.interfaces = {
    wg0 = {
      address = [ "10.0.0.2/24" ];
      dns = [ "127.0.0.1" ];
      privateKeyFile = "/home/user/.config/wg/private";
      autostart = false;

      peers = [
        {
          publicKey = "Ug3iCSRuXkvjtpsa/bQFlA3ORsFBmE07iTDino2mTD8=";
          allowedIPs = [ "0.0.0.0/0" "::/0" ];
          endpoint = "62.171.174.139:51820";
          #persistentKeepalive = 25;
        }
      ];
    };
  };

  documentation.doc.enable = false;
  services.speechd.enable = false;

#  containers.box1 = box;
#  containers.box2 = box;
#
#  containers.box0 = {
#    config =
#      { config, pkgs, ... }:
#      {
#        system.stateVersion = "${config.system.nixos.release}";
#        environment.systemPackages = with pkgs; [
#          arp-scan dnsutils ethtool iproute2 iptables perl psmisc socat tcpdump vim
#        ];
#        networking.firewall.enable = false;
#        documentation.doc.enable = false;
#        services.speechd.enable = false;
#        networking = {
#          bridges = {
#            "br-lan" = { interfaces = []; };
#          };
#          interfaces.br-lan.ipv4.addresses = [ {
#            address = "10.0.0.1";
#            prefixLength = 24;
#          } ];
#          interfaces.host0.useDHCP = true;
#        };
#        services.dnsmasq = {
#          enable = true;
#          settings = {
#            server = [ "8.8.8.8" ];
#            domain-needed = true;
#            bogus-priv = true;
#            no-resolv = true;
#            dhcp-range = [ "br-lan,10.0.0.100,10.0.0.200" ];
#            interface = "br-lan";
#          };
#        };
#      };
#    ephemeral = true;
#    privateNetwork = true;
#    extraFlags = [ "--network-veth" ];
#  };
#
#  services.dnsmasq = {
#    enable = true;
#    settings = {
#      server = [ "8.8.8.8" ];
#      domain-needed = true;
#      bogus-priv = true;
#      no-resolv = true;
#      dhcp-range = [ "ve-box0,203.0.113.100,203.0.113.200" ];
#      interface = "ve-box0";
#    };
#  };

  services.mysql.enable = false;
  services.mysql.package = pkgs.mariadb;
  services.mysql.ensureUsers = [ {
    name = "user"; ensurePermissions = { "*.*" = "ALL PRIVILEGES"; };
  } ];

  services.avahi.enable = false;

  hardware.bluetooth.enable = true;
}
