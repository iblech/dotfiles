#!/usr/bin/env perl

use warnings;
use strict;

my %dotfiles = (
    bashrc              => glob("~/.bashrc"),
    screenrc            => glob("~/.screenrc"),
    vimrc               => glob("~/.vimrc"),
    "xmonad.hs"         => glob("~/.xmonad/xmonad.hs"),
    "configuration.nix" => "/etc/nixos/configuration.nix",
);

chdir glob("~/dotfiles") or die $!;
-e "sync.pl" or die $!;

for my $name (sort keys %dotfiles) {
    printf "%-20s ", $name;

    if(system("diff", "-u", "--", $name, $dotfiles{$name}) == 0) {
        print "in sync\n";
    } else {
        if(-M $name < -M $dotfiles{$name}) {
            print "git -> system\n";
            print "please confirm: ";
            <STDIN>;
            system("cp", "--preserve=timestamps", "--", $name, $dotfiles{$name}) == 0 or die $!;
        } elsif(-M $name > -M $dotfiles{$name}) {
            print "system -> git\n";
            system("cp", "--preserve=timestamps", "--", $dotfiles{$name}, $name) == 0 or die $!;
        } else {
            print "?? (equal timestamps, but different contents)\n";
        }
    }
}
