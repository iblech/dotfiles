# Zum Erstellen des Images:
# nix-build '<nixpkgs/nixos>' -A config.system.build.isoImage -I nixos-config=iso-mathecamp.nix

{config, pkgs, ...}:

let
   kara = pkgs.callPackage /tmp/kara.nix {};
in
{
  imports = [
    <nixpkgs/nixos/modules/installer/cd-dvd/installation-cd-minimal.nix>
    <nixpkgs/nixos/modules/installer/cd-dvd/channel.nix>
  ];

  services.journald.extraConfig = "Storage=volatile";
  boot.kernel.sysctl = { "vm.dirty_writeback_centisecs" = 6000; };
  boot.kernelPackages = pkgs.linuxPackages_latest;
  boot.blacklistedKernelModules = [ "dvb_usb_rtl28xxu" ];

  networking = {
    hostName = "ada-lovelace";
    firewall.enable = false;
    networkmanager.enable = true;
    networkmanager.wifi.powersave = true;
    wireless.enable = false;
  };

  i18n = {
    consoleKeyMap = "de";
    defaultLocale = "de_DE.UTF-8";
  };

  time.timeZone = "Europe/Berlin";
  services.timesyncd.servers = [ "ntp1.uni-augsburg.de" "0.nixos.pool.ntp.org" ];

  hardware.pulseaudio.enable = true;

  environment.systemPackages = with pkgs; [
    bc
    (texlive.combine {
      inherit (texlive) scheme-medium biblatex logreq xstring csquotes tabto-ltx
      soul xypic libertine chngcntr multirow bussproofs breakurl mweights fontaxes;
    })
    djview
    # geogebra
    maxima
    # scilab
    golly
    vlc
    mpv
    xjump
    # tuxpaint
    alacritty
    neverball
    moon-buggy
    # liquidwar
    kobodeluxe
    gtypist
    extremetuxracer
    bastet
    sonic-pi
    fftw
    chromium
    dump1090
    feh
    file
    firefoxWrapper
    gcc
    ghc
    gimp
    git
    gnome3.gedit
    gnumake
    imagemagick
    iptables
    jre
    kara
    kate
    libav
    libreoffice
    lsof
    lxpanel
    manpages
    mosh
    mpv
    mupdf
    neovim
    # net-tools
    networkmanagerapplet
    nmap
    nmap
    parallel
    pavucontrol
    pciutils
    pingus
    powertop
    psmisc
    (pkgs.python3.withPackages (self: [ self.pygame self.numpy self.matplotlib ]))
    redshift
    rfkill
    stellarium
    tig
    tor-browser-bundle-bin
    vim
    xcalib
    xdotool
    xorg.xev
    xorg.xgamma
    xorg.xkill
    xorg.xmodmap
    xorg.xwininfo
    xrandr-invert-colors
    xsel
    xvkbd
    fish
  ];

  fonts.fonts = with pkgs; [
    hack-font
    ubuntu_font_family
  ];

  environment.variables = {
    EDITOR = "vim";
  };

  users.users.ada = {
    isNormalUser = true;
    description = "Ada Lovelace";
    createHome = true;
    home = "/home/ada";
    uid = 1000;
    initialHashedPassword = "$6$utLZPDNys$nxpqRBobo7NAi9kFs7J8Ar5UN2zJY97.tuavJyk1ACyVoELeUwS3AtU7eCPq.R3Yxtb3GvmpuOuH0xrww0pdp.";
    shell = pkgs.fish;
  };

  services.openssh.enable = true;
  services.redshift = {
    enable = true;
    latitude = "48.4505";
    longitude = "10.5731";
  };

  services.xserver = {
    enable = true;
    layout = "de";
    xkbVariant = "nodeadkeys";
    desktopManager.xfce.enable = true;
    displayManager = {
      slim = {
        enable = true;
        defaultUser = "ada";
        autoLogin = true;
      };
    };
    libinput = {
      enable = true;
      middleEmulation = true;
    };
  };
}
