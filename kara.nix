{ stdenv, fetchurl, jre, bash }:

stdenv.mkDerivation rec {
  version = "1.0";
  name = "kara-${version}";
  src = fetchurl {
    url = "https://www.swisseduc.ch/informatik/karatojava/kara/classes/kara.jar";
    sha256 = "0q1a5qax20dn9rkay8zhpx6w6ickxapdv9czsk5wsjayfz10lkqq";
  };
  buildInputs = [ ];

  #unpackCmd = "true";

  phases = [ "installPhase" ];
  #phases = [ "buildPhase" "installPhase" "fixupPhase" "distPhase" ];

  installPhase = ''
    echo "#!${bash}/bin/bash" > kara
    echo "${jre}/bin/java -jar ${src}" >> kara
    mkdir -p $out/bin
    cp kara $out/bin
    chmod a+x $out/bin/kara
  '';

 meta = with stdenv.lib; {
   description = "Creates a cute";
   longDescription = ''
   Oneko changes your mouse cursor into a mouse
   and creates a little cute cat, which starts
   chasing around your mouse cursor.
   When the cat is done catching the mouse, it starts sleeping.
   '';
   homepage = "http://www.daidouji.com/oneko/";
   license = licenses.publicDomain;
   maintainers = [ maintainers.xaverdh ];
   platforms = platforms.unix;
 };
}
